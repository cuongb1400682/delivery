import React from "react";
import {createStackNavigator} from "react-navigation";

import {navigatorStyles} from "../styles/navigatorStyles";
import screenIds from "src/constants/screenIds";
import UserContact from "src/screens/UserAccount/containers/UserContact/UserContact";
import UserOverview from "src/screens/UserAccount/containers/UserOverview/UserOverview";
import UserLocation from "src/screens/UserAccount/containers/UserLocation/UserLocation";
import Header from "src/containers/Header/Header";

const routeConfigs = {
    [screenIds.USER_ACCOUNT_OVERVIEW]: {
        screen: UserOverview
    },
    [screenIds.USER_ACCOUNT_CONTACT]: {
        screen: UserContact
    },
    [screenIds.USER_ACCOUNT_LOCATION]: {
        screen: UserLocation
    },
};

export default createStackNavigator(routeConfigs, {
    initialRouteName: screenIds.USER_ACCOUNT_OVERVIEW,
    navigationOptions: {
        headerStyle: navigatorStyles.hideSystemDefaultHeader,
        header: () => <Header/>
    }
});
