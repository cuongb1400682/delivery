import React from "react";
import {createSwitchNavigator} from "react-navigation";

import screenIds from "src/constants/screenIds"
import Login from "src/screens/Login/Login";
import Main from "src/navigator/mainRouteConfigs";

const routeConfigs = {
    [screenIds.LOGIN]: {
        screen: Login
    },
    [screenIds.MAIN]: {
        screen: Main
    }
};

export default createSwitchNavigator(routeConfigs, {
    initialRouteName: screenIds.LOGIN,
});
