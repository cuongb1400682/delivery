import React from "react";
import {createBottomTabNavigator} from "react-navigation";

import {navigatorStyles} from "../styles/navigatorStyles";
import Orders from "./ordersRouteConfigs";
import screenIds from "src/constants/screenIds";
import colors from "src/constants/colors";
import UserAccountTabIcon from "src/components/base/SvgIcons/UserAccountTabIcon/UserAccountTabIcon";
import OrderTabIcon from "src/components/base/SvgIcons/OrderTabIcon/OrderTabIcon";
import Income from "src/screens/Income/Income";
import Notification from "src/screens/Notification/Notification";
import NotificationTabIcon from "src/components/base/SvgIcons/NotificationTabIcon/NotificationTabIcon";
import IncomeTabIcon from "src/components/base/SvgIcons/IncomeTabIcon/IncomeTabIcon";
import UserAccount from "src/navigator/userAccountRouteConfigs";

const routeConfigs = {
    [screenIds.ORDERS]: {
        screen: Orders,
        navigationOptions: {
            tabBarIcon: ({focused}) => <OrderTabIcon focus={focused}/>
        }
    },
    [screenIds.INCOME]: {
        screen: Income,
        navigationOptions: {
            tabBarIcon: ({focused}) => <IncomeTabIcon focus={focused}/>
        }
    },
    [screenIds.NOTIFICATION]: {
        screen: Notification,
        navigationOptions: {
            tabBarIcon: ({focused}) => <NotificationTabIcon focus={focused}/>
        }
    },
    [screenIds.USER_ACCOUNT]: {
        screen: UserAccount,
        navigationOptions: {
            tabBarIcon: ({focused}) => <UserAccountTabIcon focus={focused}/>
        }
    }
};

export default createBottomTabNavigator(routeConfigs, {
    initialRouteName: screenIds.ORDERS,
    tabBarOptions: {
        activeTintColor: colors.primaryColor,
        showLabel: false,
        style: navigatorStyles.bottomTab
    }
});
