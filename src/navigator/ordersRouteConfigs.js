import React from "react";
import {createStackNavigator} from "react-navigation";

import {navigatorStyles} from "../styles/navigatorStyles";
import screenIds from "src/constants/screenIds"
import OrdersList from "src/screens/Orders/OrdersList/OrdersList";
import OrderDetail from "src/screens/Orders/OrderDetail/OrderDetail";
import Header from "src/containers/Header/Header";
import CustomerSignature from "src/screens/Orders/CustomerSignature/CustomerSignature";

const routeConfigs = {
    [screenIds.ORDERS_LIST]: {
        screen: OrdersList
    },
    [screenIds.ORDER_DETAIL]: {
        screen: OrderDetail
    },
    [screenIds.CUSTOMER_SIGNATURE]: {
        screen: CustomerSignature
    },
};

export default createStackNavigator(routeConfigs, {
    initialRouteName: screenIds.ORDERS_LIST,
    navigationOptions: {
        headerStyle: navigatorStyles.hideSystemDefaultHeader,
        header: () => <Header/>
    }
});
