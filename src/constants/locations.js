const hoChiMinh = {
    latitude: 10.8231,
    longitude: 106.6297,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
};

export {
    hoChiMinh
};
