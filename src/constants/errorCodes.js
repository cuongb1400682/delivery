const errorCodes = {
    errorLoadingFailedToLoadData: "error_fail_to_load_data",
    errorUndefinedSVGRef: "error_undefined_svg_ref",
    errorMethodToDataURLNotFound: "error_method_to_data_url_not_found"
};

export {
    errorCodes
};
