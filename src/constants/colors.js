export default {
    primaryColor: "#ef4323",
    primaryLight: "#fff",
    primaryDark: "#000",
    textColor: "#222",

    creationStepColor: "#dedb00",
    pickingStepColor: "#e10000",
    shippingStepColor: "#0275D8",
    completedStepColor: "#5CB85C",
    cancelStepColor: "#999",
    refundStepColor: "#999",
    returnedStepColor: "#999",
};
