const SOListTimeSortType = {
    CREATED: "CREATED",
    MODIFIED: "MODIFIED"
};

const OrderSources = {
    FACEBOOK: "FACEBOOK",
    PHONE: "PHONE",
    STORE: "STORE"
};

const SOStatus = {
    DRAFT: "DRAFT",
    PENDING: "PENDING",
    PROCESSING: "PROCESSING",
    VERIFIED: "VERIFIED",
    PICKING: "PICKING",
    PACKED: "PACKED",
    DROPSHIP: "DROPSHIP",
    SHIPPING: "SHIPPING",
    RETURNED: "RETURNED",
    COMPLETED: "COMPLETED",
    REFUND: "REFUND",
    CANCEL: "CANCEL"
};

const paymentMethods = {
    COD: "COD",
    CASH: "CASH",
    NAPAS: "NAPAS",
    ONLINE_PAYMENT: "ONLINE_PAYMENT",
    INTERNET_BANKING: "INTERNET_BANKING",
    VISA_MASTER_CARD: "VISA_MASTER_CARD"
};

const shippingMethods = {
    STANDARD: "STANDARD",
    FAST: "FAST"
};

const orderManagerSteps = {
    CREATION: "CREATION",
    PICKING: "PICKING",
    SHIPPING: "SHIPPING",
    COMPLETED: "COMPLETED",
    CANCEL: "CANCEL",
    REFUND: "REFUND",
    RETURNED: "RETURNED"
};

const paymentStatus = {
    PAID: "PAID",
    UNPAID: "UNPAID",
    REFUND: "REFUND"
};

export {
    SOListTimeSortType,
    OrderSources,
    SOStatus,
    shippingMethods,
    orderManagerSteps,
    paymentMethods,
    paymentStatus
};
