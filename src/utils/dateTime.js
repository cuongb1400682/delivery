import {addLeadingZeros} from "./strings";

const startOfDay = () => {
    const start = new Date();
    start.setDate(start.getDate() - 3);// todo: this is fake date
    start.setHours(0, 0, 0, 0);
    return start;
};

const endOfDay = () => {
    const end = new Date();
    end.setHours(29, 59, 59, 999);
    return end;
};

const toShortDateDisplay = (dt) => {
    if (!dt) {
        return "";
    }

    const dateTime = new Date(dt);
    const date = addLeadingZeros(dateTime.getDate());
    const month = addLeadingZeros(dateTime.getMonth() + 1);
    const year = addLeadingZeros(dateTime.getFullYear());

    return `${date}/${month}/${year}`;
};

export {
    toShortDateDisplay,
    startOfDay,
    endOfDay
};
