//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/entries
const objectEntries = object => Object.keys(object).map(key => ([key, object[key]]))

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/values
const objectValues = object => {
    if (typeof object !== "object") {
        return []
    }

    return Object.keys(object).map(key => object[key])
}

/*
   Return "true" if two object are same key, value and order
   Reference: https://stackoverflow.com/questions/1068834/object-comparison-in-javascript#answer-1144249
 */
const objectCompare = (firstObject, secondObject) => JSON.stringify(firstObject) === JSON.stringify(secondObject)

const objectRemoveFalsyValue = object => {
    if (typeof object !== "object") {
        console.error("Wrong object type") //TODO: Put error to Sentry
    }
    const keys = Object.keys(object)

    keys.forEach(key => {
        if (!object[key]) {
            delete object[key]
        }
    })
}

export {objectEntries, objectValues, objectCompare, objectRemoveFalsyValue}
