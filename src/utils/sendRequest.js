import {getAccessToken} from "src/utils/userAuth";
import configs from "src/configs/configs";
import {stringifyQuery} from "./queryString";

const makeFetchHeaders = (headers) => ({
    "x-giigaa-client-id": "0000",
    "x-giigaa-client-type": 3,
    "x-giigaa-app-type": "INSIDE",
    "x-giigaa-client-version": 1234,
    "x-giigaa-api-version": 1,
    "x-giigaa-access-token": getAccessToken(),
    ...headers
});

const makeFetchRequestUrl = ({url, isAuth, params}) => {
    const apiUrl = isAuth ? "AUTH_ROOT_URL" : "INSIDE_BACKEND_URL";
    const searchString = stringifyQuery(params);
    const requestUrl = configs[apiUrl] + url;

    return searchString ? `${requestUrl}?${searchString}` : requestUrl;
};

const hasJSONBody = (body) => !!body && !(body instanceof URLSearchParams || body instanceof FormData);

const sendRequest = async ({url = "", method = "GET", headers = {}, params = {}, body, isAuth = false}) => {
    const requestUrl = makeFetchRequestUrl({
        url,
        isAuth,
        params
    });
    const requestHeaders = makeFetchHeaders(headers);
    let requestBody = body;


    if (hasJSONBody(body)) {
        requestHeaders["Content-Type"] = "application/json; charset=UTF-8";
        requestBody = JSON.stringify(body);
    }

    const fetchRequestParams = {
        method: method || "GET",
        headers: requestHeaders,
        body: requestBody,
        credentials: "same-origin"
    };

    if (method === "GET") {
        delete fetchRequestParams.body;
    }

    // call fetch api to fetch data from server
    return await fetch(requestUrl, fetchRequestParams);
};

export {
    sendRequest
};
