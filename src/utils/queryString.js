import {objectEntries} from "src/utils/object"

const DEFAULT_PARAMS = {
    page_size: 50
}

const isHeuristicEncoded = value => decodeURIComponent(value) !== value

const encodeValue = value => isHeuristicEncoded(value) ? value : encodeURIComponent(value)

const encodeKeyValue = ([name, value]) => name + '=' + encodeValue(value)

const convertToObject = (params, [key, value]) => ({
    ...params,
    [key]: decodeURIComponent(value)
})

const filterNullAndEmptyValues = ([, value]) => {
    if (Array.isArray(value)) {
        return value.length
    }

    return value !== null && value !== '' && value !== undefined
}

const filterDefaultValues = ([key, value]) => DEFAULT_PARAMS[key] !== value

const stringifyQuery = (params = {}) => {
    return objectEntries(params)
        .filter(filterNullAndEmptyValues)
        .filter(filterDefaultValues)
        .map(encodeKeyValue)
        .join('&')
}

const parseQueryString = queryString => {
    if (queryString && queryString !== '?') {
        return queryString
            .substring(1)
            .split('&')
            .map(pair => pair.split('='))
            .reduce(convertToObject, {}) || {}
    }

    return {}
}

export {stringifyQuery, parseQueryString}
