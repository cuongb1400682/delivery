import {orderManagerSteps, paymentStatus, SOStatus} from "src/constants/enums";

const mapFromSOStatusToOrderManagerStep = (status) => ({
    [SOStatus.DRAFT]: orderManagerSteps.CREATION,
    [SOStatus.PENDING]: orderManagerSteps.CREATION,
    [SOStatus.PROCESSING]: orderManagerSteps.CREATION,
    [SOStatus.VERIFIED]: orderManagerSteps.CREATION,
    [SOStatus.PICKING]: orderManagerSteps.PICKING,
    [SOStatus.PACKED]: orderManagerSteps.PICKING,
    [SOStatus.DROPSHIP]: orderManagerSteps.SHIPPING,
    [SOStatus.SHIPPING]: orderManagerSteps.SHIPPING,
    [SOStatus.RETURNED]: orderManagerSteps.RETURNED,
    [SOStatus.REFUND]: orderManagerSteps.REFUND,
    [SOStatus.COMPLETED]: orderManagerSteps.COMPLETED,
    [SOStatus.CANCEL]: orderManagerSteps.CANCEL
})[status] || status;

const convertToOrderManagerStep = ({status = "", order_number = ""}) => {
    if (order_number.startsWith("RF_")) {
        return orderManagerSteps.REFUND;
    }

    return mapFromSOStatusToOrderManagerStep(status);
};

const convertOrderStepToPaymentStatus = (orderStep = "") => ({
    [orderManagerSteps.CREATION]: paymentStatus.UNPAID,
    [orderManagerSteps.PICKING]: paymentStatus.UNPAID,
    [orderManagerSteps.SHIPPING]: paymentStatus.UNPAID,
    [orderManagerSteps.COMPLETED]: paymentStatus.PAID,
    [orderManagerSteps.CANCEL]: paymentStatus.UNPAID,
    [orderManagerSteps.REFUND]: paymentStatus.REFUND,
    [orderManagerSteps.RETURNED]: paymentStatus.PAID
})[orderStep] || paymentStatus.UNPAID;

const addOrderStepToOrdersList = (orders = []) => orders.map(order => ({
    ...order,
    order_step: convertToOrderManagerStep(order)
}));

const reduceStatusHistory = ({status_history = []}) => {
    let newHistory = [];
    let lastStep = "";

    status_history.forEach(({status, created_time}) => {
        const step = convertToOrderManagerStep({status});

        if (step !== lastStep) {
            newHistory.push({
                step: step,
                created_time
            });
        }

        lastStep = step;
    });

    return newHistory;
};

export {
    convertOrderStepToPaymentStatus,
    convertToOrderManagerStep,
    reduceStatusHistory,
    addOrderStepToOrdersList
};
