import {sendRequest} from "src/utils/sendRequest";

const toRequest = type => `${type}_REQUEST`;
const toSuccess = type => `${type}_SUCCESS`;
const toFailure = type => `${type}_FAILURE`;

const makeAction = transform => (
    {type, payload, meta},
    response
) => ({
    type: transform(type),
    payload: response || payload,
    meta: {...(meta || {})}
});

const makeRequestAction = makeAction(toRequest);
const makeSuccessAction = makeAction(toSuccess);
const makeFailureAction = makeAction(toFailure);

const parseJsonResponse = str => {
    if (typeof str !== "string") {
        return {};
    }

    try {
        return JSON.parse(str);
    } catch (e) {
        return "";
    }
};

const createAction = action => (dispatch, getState) => {
    const {type, payload, meta = {}, onSuccess} = action;

    if (!payload) {
        console.error("The payload must not be empty");
        return false;
    }

    dispatch({type, payload, meta});

    if (typeof onSuccess === "function") {
        onSuccess(dispatch, getState);
    }
};

const validateResponse = response => {
    if (!response.ok) {
        throw new Error(JSON.stringify(response));
    }

    return response;
};

const createAsyncAction = action => async dispatch => {
    const {payload, onSuccess, onError} = action;

    if (!payload || typeof payload.request !== "string") {
        console.error("The request url does not exist");
        return false;
    }

    dispatch(makeRequestAction(action));

    const {request, method, body, params, options, headers} = payload;

    try {
        const response = await sendRequest({
            url: request,
            method,
            body,
            params,
            headers,
            ...options
        });

        if (typeof onSuccess === "function") {
            onSuccess(dispatch, response);
        }

        validateResponse(response);

        return dispatch(makeSuccessAction(action, await response.json()));
    } catch (error) {
        if (typeof onError === "function") {
            onError(dispatch, parseJsonResponse(error.message));
        }

        return dispatch(makeFailureAction(action, parseJsonResponse(error.message)));
    }
};

export {
    toRequest,
    toSuccess,
    toFailure,
    createAction,
    createAsyncAction
};
