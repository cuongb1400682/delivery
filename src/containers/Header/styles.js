import {StyleSheet} from "react-native";

const HEADER_HEIGHT = 66;

const styles = StyleSheet.create({
    header: {
        marginBottom: HEADER_HEIGHT
    },
    linearBackground: {
        flex: 1
    }
});

export {
    styles
};
