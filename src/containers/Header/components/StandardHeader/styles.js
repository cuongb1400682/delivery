import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    standardHeader: {
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    buttonIcon: {
        fontSize: 24,
        color: "#fff",
        marginRight: 10
    },
    doubleCheckIcon: {
        marginRight: 10
    },
    text: {
        color: "#fff"
    },
    headerButtonTouchable: {
        position: "absolute",
        left: 0,
        top: 0
    },
    headerButton: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        height: 50,
        paddingHorizontal: 16,
    },
    headerTitle: {
        fontWeight: "bold",
        fontSize: 15
    }
});

export {
    styles
};
