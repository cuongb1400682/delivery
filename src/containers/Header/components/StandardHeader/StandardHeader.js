import React, {Component} from "react";
import {Text, View} from "react-native";
import {FontAwesome} from "@expo/vector-icons";
import PropTypes from "prop-types";
import {withNavigation} from "react-navigation";

import {styles} from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {tr} from "src/localization/localization";

@withNavigation
class StandardHeader extends Component {
    static propTypes = {
        onBack: PropTypes.func,
        title: PropTypes.string,
        showBackButton: PropTypes.bool
    };

    static defaultProps = {
        onBack: null,
        title: "",
        showBackButton: true
    };

    onBack = () => {
        if (this.props.onBack) {
            this.props.onBack();
        } else {
            this.props.navigation.goBack();
        }
    };

    render() {
        return (
            <View style={styles.standardHeader}>
                {this.props.showBackButton &&
                <Touchable style={styles.headerButtonTouchable} onPress={this.onBack}>
                    <View style={styles.headerButton}>
                        <FontAwesome style={styles.buttonIcon} name="angle-left"/>
                        <Text style={styles.text}>{tr("standard_header_back_button")}</Text>
                    </View>
                </Touchable>}
                <Text style={[styles.text, styles.headerTitle]}>
                    {this.props.title}
                </Text>
            </View>
        );
    }
}

export default StandardHeader;
