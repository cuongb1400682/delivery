import React, {Component} from "react";
import {Text, View} from "react-native";
import {withNavigation} from "react-navigation";
import {Svg} from "expo";

import {styles} from "./styles";
import {tr} from "src/localization/localization";
import {usefulStyles} from "src/styles/usefulStyles";
import GiigaaLinearGradient from "src/components/base/GiigaaLinearGradient/GiigaaLinearGradient";

const Avatar = () => (
    <Svg width="60" height="60" viewBox="0 0 60 60" fill="none" xmlns="http://www.w3.org/2000/svg">
        <Svg.Circle cx="30" cy="30" r="30" fill="white" fillOpacity="0.4"/>
        <Svg.Path
            d="M30.2018 46C25.2242 46 20.2466 46 15.2691 46H15.1345C15.1345 46 15 46 15 45.8708C15.1345 44.3208 15.8072 42.9 17.0179 41.7375C17.8251 40.9625 18.9013 40.4458 19.843 40.0583C20.9193 39.6708 21.9955 39.2833 22.9372 38.7667C23.6099 38.5083 24.2825 38.1208 24.9552 37.7333C25.4933 37.3458 25.8969 36.9583 26.0314 36.4417C26.1659 36.1833 26.1659 36.0542 26.1659 35.7958C26.1659 34.8917 26.1659 34.1167 26.1659 33.2125C26.1659 33.0833 26.1659 33.0833 26.0314 32.9542C25.7623 32.6958 25.4933 32.4375 25.3587 32.1792C24.8206 31.4042 24.5516 30.5 24.417 29.5958C24.417 29.4667 24.2825 29.3375 24.148 29.2083C23.7444 29.0792 23.6099 28.8208 23.3408 28.5625C23.0717 28.175 23.0717 27.6583 22.9372 27.2708C22.9372 26.8833 22.9372 26.4958 23.0717 26.1083C23.3408 26.1083 23.4753 25.9792 23.7444 25.85V25.7208C23.6099 25.2042 23.4753 24.6875 23.3408 24.1708C23.2063 23.1375 23.0717 22.2333 23.0717 21.2C23.0717 20.2958 23.2063 19.3917 23.6099 18.6167C24.148 17.0667 25.3587 16.0333 26.9731 15.5167C27.9148 15.1292 28.8565 15 29.9327 15C30.7399 15 31.6816 15 32.3543 15.2583C32.8924 15.3875 33.4305 15.775 33.6996 16.1625C33.6996 16.2917 33.8341 16.2917 33.8341 16.2917C34.6413 16.4208 35.4484 16.6792 36.1211 17.325C36.6592 17.8417 37.0628 18.4875 37.1973 19.2625C37.4664 20.0375 37.4664 20.9417 37.4664 21.7167C37.3318 23.0083 37.0628 24.4292 36.6592 25.5917V25.7208C37.0628 25.9792 37.1973 26.3667 37.1973 26.8833C37.1973 27.6583 37.0628 28.3042 36.6592 28.95C36.5247 29.2083 36.2556 29.4667 35.852 29.4667C35.7175 29.4667 35.7175 29.4667 35.7175 29.5958C35.583 30.3708 35.3139 31.1458 35.0448 31.7917C34.7758 32.1792 34.6413 32.5667 34.2377 32.9542C34.1031 33.0833 34.1031 33.0833 33.9686 33.2125C33.8341 33.2125 33.8341 33.3417 33.8341 33.3417C33.8341 34.1167 33.8341 35.0208 33.8341 35.7958C33.8341 36.7 34.2377 37.2167 34.9103 37.7333C35.4484 38.25 36.2556 38.5083 36.9283 38.7667C37.87 39.1542 38.9462 39.5417 39.8879 39.9292C40.8296 40.3167 41.6368 40.7042 42.4439 41.2208C43.3857 41.8667 44.0583 42.6417 44.4619 43.5458C44.7309 44.1917 44.8655 44.8375 45 45.4833C45 45.6125 45 45.7417 45 45.8708C45 46 45 46 44.8655 46H44.7309C40.1569 46 35.1794 46 30.2018 46Z"
            fill="white"
        />
    </Svg>
);

@withNavigation
class UserAccountHeader extends Component {
    static propTypes = {};

    static defaultProps = {};

    render() {
        return (
            <View style={styles.userAccountHeader}>
                <View style={[styles.upperPart, usefulStyles.flexColumnCenter]}>
                    <Text style={[styles.text, styles.headerTitle]}>
                        {tr("user_account_header_title")}
                    </Text>
                </View>
                <View style={styles.lowerPart}>
                    <Avatar/>
                    <View style={styles.userInformation}>
                        <Text style={[styles.text, styles.userName]}>Nguyễn Công Thành</Text>
                        <Text style={[styles.text, styles.userID]}>ID: 98793745</Text>
                    </View>
                    <GiigaaLinearGradient style={styles.workingType}>
                        <Text style={[styles.text, styles.workingTypeLabel]}>Full time</Text>
                    </GiigaaLinearGradient>
                </View>
            </View>
        );
    }
}

export default UserAccountHeader;
