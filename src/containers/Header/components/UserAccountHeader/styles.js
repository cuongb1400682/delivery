import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    userAccountHeader: {
        height: 136,
        flexDirection: "column",
    },
    headerTitle: {
        fontWeight: "bold",
        fontSize: 15
    },
    text: {
        color: "#fff",
        fontSize: 13
    },
    upperPart: {
        height: 50
    },
    lowerPart: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 16
    },
    userInformation: {
        marginLeft: 16
    },
    userName: {
        fontWeight: "bold"
    },
    userID: {
        marginTop: 3
    },
    workingType: {
        paddingHorizontal: 16,
        paddingVertical: 8,
        borderStyle: "solid",
        borderWidth: 1,
        borderColor: "#fff",
        borderRadius: 5,
        position: "absolute",
        right: 16
    },
    workingTypeLabel: {
    }
});

export {
    styles
};
