import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    headerButtonsContainer: {
        flex: 1,
        height: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingLeft: 24,
        paddingRight: 2,
    },
    rightButtonGroup: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    headerButton: {
        width: 50,
        height: 50,
        borderRadius: 50,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    }
});

export {
    styles
};
