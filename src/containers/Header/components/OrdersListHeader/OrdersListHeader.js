import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

import {styles} from "./styles";
import OnlineIndicator from "src/components/base/SvgIcons/OnlineIndicator/OnlineIndicator";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import EmergencyPhoneCall from "src/components/base/SvgIcons/EmergencyPhoneCall/EmergencyPhoneCall";
import LocationMark from "src/components/base/SvgIcons/LocationMark/LocationMark";
import BarcodeGun from "src/components/base/SvgIcons/BarcodeGun/BarcodeGun";

class OrdersListHeader extends Component {
    static propTypes = {
        onMakePhoneCall: PropTypes.func,
        onShowMap: PropTypes.func,
        onScanBarcode: PropTypes.func
    };

    static defaultProps = {
        onMakePhoneCall: () => {
        },
        onShowMap: () => {
        },
        onScanBarcode: () => {
        }
    };

    render() {
        return (
            <View style={styles.headerButtonsContainer}>
                <OnlineIndicator/>
                <View style={styles.rightButtonGroup}>
                    <Touchable style={styles.headerButton} onPress={this.props.onMakePhoneCall}>
                        <EmergencyPhoneCall/>
                    </Touchable>
                    <Touchable style={styles.headerButton} onPress={this.props.onShowMap}>
                        <LocationMark/>
                    </Touchable>
                    <Touchable style={styles.headerButton} onPress={this.props.onScanBarcode}>
                        <BarcodeGun/>
                    </Touchable>
                </View>
            </View>
        );
    }
}

export default OrdersListHeader;
