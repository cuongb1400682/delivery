import React, {Component} from "react";
import {withNavigation} from "react-navigation";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import {styles} from "./styles";
import {getStatusBarHeight} from "src/utils/statusBar";
import StandardHeader from "src/containers/Header/components/StandardHeader/StandardHeader";
import screenIds from "src/constants/screenIds";
import OrdersListHeader from "src/containers/Header/components/OrdersListHeader/OrdersListHeader";
import {
    hideDestinationsMap,
    hideSupportingCalls,
    showDestinationsMap,
    showSupportingCalls
} from "src/redux/common/commonActions";
import {tr} from "src/localization/localization";
import UserAccountHeader from "src/containers/Header/components/UserAccountHeader/UserAccountHeader";
import GiigaaLinearGradient from "src/components/base/GiigaaLinearGradient/GiigaaLinearGradient";

@withNavigation
class Header extends Component {
    static propTypes = {
        isSupportingCallVisible: PropTypes.bool,
        isDestinationsMapVisible: PropTypes.bool,
        showSupportingCalls: PropTypes.func,
        hideSupportingCalls: PropTypes.func,
        showDestinationsMap: PropTypes.func,
        hideDestinationsMap: PropTypes.func,
    };

    static defaultProps = {
        isSupportingCallVisible: false,
        isDestinationsMapVisible: false,
        showSupportingCalls: () => {
        },
        hideSupportingCalls: () => {
        },
        showDestinationsMap: () => {
        },
        hideDestinationsMap: () => {
        },
    };

    toggleSupportingCall = () => {
        if (this.props.isSupportingCallVisible) {
            this.props.hideSupportingCalls();
        } else {
            this.props.showSupportingCalls();
        }
    };

    toggleDestinationsMap = () => {
        if (this.props.isDestinationsMapVisible) {
            this.props.hideDestinationsMap();
        } else {
            this.props.showDestinationsMap();
        }
    };

    renderOrderDetailHeader = () => <StandardHeader title={tr("order_detail_header_title")}/>;

    renderOrdersListHeader = () => (
        <OrdersListHeader
            onMakePhoneCall={this.toggleSupportingCall}
            onShowMap={this.toggleDestinationsMap}
        />
    );

    renderCustomerSignatureHeader = () => <StandardHeader title={tr("customer_signature_header_title")}/>;

    renderUserAccountOverviewHeader = () => <UserAccountHeader/>

    renderRelevantHeader = () => {
        const {state = {}} = this.props.navigation;

        return ({
            [screenIds.ORDER_DETAIL]: this.renderOrderDetailHeader(),
            [screenIds.ORDERS_LIST]: this.renderOrdersListHeader(),
            [screenIds.CUSTOMER_SIGNATURE]: this.renderCustomerSignatureHeader(),
            [screenIds.USER_ACCOUNT_OVERVIEW]: this.renderUserAccountOverviewHeader()
        })[state.routeName] || null;
    };

    render() {
        const skipStatusBarHeight = {
            paddingTop: getStatusBarHeight()
        };

        return (
            <GiigaaLinearGradient style={[styles.linearBackground, skipStatusBarHeight]}>
                {this.renderRelevantHeader()}
            </GiigaaLinearGradient>
        );
    }
}

const mapStateToProps = (state) => ({
    isSupportingCallVisible: state.common.supporting_calls_visible,
    isDestinationsMapVisible: state.common.all_destination_visible
});

export default connect(mapStateToProps, {
    showSupportingCalls,
    hideSupportingCalls,
    showDestinationsMap,
    hideDestinationsMap
})(Header);
