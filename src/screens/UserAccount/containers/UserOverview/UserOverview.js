import React, {Component} from "react";
import {Switch, Text, View} from "react-native";
import {SimpleLineIcons} from "@expo/vector-icons";
import {connect} from "react-redux";
import {withNavigation} from "react-navigation";

import {styles} from "./styles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {tr} from "src/localization/localization";
import UserAccountMenuIcon from "src/screens/UserAccount/components/UserAccountMenuIcon/UserAccountMenuIcon";

@withNavigation
class UserOverview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOnline: true
        };
    }

    toggleOnlineState = () => {
        this.setState({
            isOnline: !this.state.isOnline
        });
    };

    render() {
        const workingTime = "6 tháng";
        return (
            <View style={styles.accountMenu}>
                <Touchable style={styles.accountMenuItem}>
                    <View style={styles.menuItemIcon}>
                        <View
                            style={[
                                styles.onlineIndicator,
                                this.state.isOnline ? styles.onlineIndicatorActive : styles.onlineIndicatorDeactive
                            ]}
                        />
                    </View>
                    <Text style={styles.menuItemText}>
                        {tr(this.state.isOnline
                            ? "user_overview_online"
                            : "user_overview_offline")}
                    </Text>
                    <View style={styles.menuItemRightButton}>
                        <Switch value={this.state.isOnline} onValueChange={this.toggleOnlineState}/>
                    </View>
                </Touchable>
                <Touchable style={styles.accountMenuItem}>
                    <View style={styles.menuItemIcon}>
                        <UserAccountMenuIcon type="calendar"/>
                    </View>
                    <Text style={styles.menuItemText}>
                        {tr("user_overview_working_time")}: {workingTime}
                    </Text>
                    <View style={styles.menuItemRightButton}>
                        <SimpleLineIcons color="#8c8c8c" name="arrow-right"/>
                    </View>
                </Touchable>
                <Touchable style={styles.accountMenuItem}>
                    <View style={styles.menuItemIcon}>
                        <UserAccountMenuIcon type="info"/>
                    </View>
                    <Text style={styles.menuItemText}>
                        {tr("user_overview_contact_info")}
                    </Text>
                    <View style={styles.menuItemRightButton}>
                        <SimpleLineIcons color="#8c8c8c" name="arrow-right"/>
                    </View>
                </Touchable>
                <Touchable style={styles.accountMenuItem}>
                    <View style={styles.menuItemIcon}>
                        <UserAccountMenuIcon type="marker"/>
                    </View>
                    <Text style={styles.menuItemText}>
                        {tr("user_overview_update_my_location")}
                    </Text>
                    <View style={styles.menuItemRightButton}>
                        <SimpleLineIcons color="#8c8c8c" name="arrow-right"/>
                    </View>
                </Touchable>
            </View>
        );
    }
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {})(UserOverview);
