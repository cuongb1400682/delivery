import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    onlineIndicator: {
        height: 12,
        width: 12,
        borderRadius: 12,
    },
    onlineIndicatorActive: {
        backgroundColor: "#4bd562"
    },
    onlineIndicatorDeactive: {
        backgroundColor: "#8c8c8c"
    },
    accountMenu: {
        flexDirection: "column"
    },
    accountMenuItem: {
        height: 60,
        alignSelf: "stretch",
        backgroundColor: "#fff",
        borderStyle: "solid",
        borderBottomWidth: 1,
        borderColor: "#ddd",
        flexDirection: "row",
        alignItems: "center"
    },
    menuItemIcon: {
        width: 60,
        height: 60,
        justifyContent: "center",
        alignItems: "center"
    },
    menuItemIconChar: {
        fontSize: 22,
        color: "#8c8c8c"
    },
    menuItemText: {
        color: "#8c8c8c"
    },
    menuItemRightButton: {
        position: "absolute",
        right: 16
    },
});

export {
    styles
};
