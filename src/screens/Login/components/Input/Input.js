import React, {Component} from "react";
import {TextInput, TouchableWithoutFeedback, View} from "react-native";
import PropTypes from "prop-types";

import {styles} from "./styles";
import InputIcon from "src/screens/Login/components/InputIcon/InputIcon";

class Input extends Component {
    static propTypes = {
        value: PropTypes.string,
        defaultValue: PropTypes.string,
        placeholder: PropTypes.string,
        iconType: PropTypes.string,
        inputContainerStyle: PropTypes.any,
        onChange: PropTypes.func,
        secureTextEntry: PropTypes.bool
    };

    static defaultProps = {
        value: "",
        defaultValue: "",
        placeholder: "",
        iconType: "",
        inputContainerStyle: {},
        onChange: () => {
        },
        secureTextEntry: false
    };

    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        };
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.state.value) {
            this.setState({
                value: this.state.value
            });
        }
    }

    onChange = (value) => {
        this.setState({
            value
        });

        this.props.onChange(value);
    };

    focusOnInput = () => {
        if (this.inputRef) {
            this.inputRef.focus();
        }
    };

    render() {
        return (
            <TouchableWithoutFeedback onPress={this.focusOnInput}>
                <View style={[this.props.inputContainerStyle, styles.inputContainer, styles.underline]}>
                    {this.props.iconType &&
                    <View style={styles.iconContainer}>
                        <InputIcon type={this.props.iconType}/>
                    </View>}
                    <TextInput
                        ref={ref => this.inputRef = ref}
                        style={styles.input}
                        placeholderTextColor="rgba(255, 255, 255, 0.8)"
                        value={this.props.value}
                        onChange={this.onChange}
                        defaultValue={this.props.defaultValue}
                        placeholder={this.props.placeholder}
                        selectionColor="rgba(255, 255, 255, 0.8)"
                        secureTextEntry={this.props.secureTextEntry}
                    />
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

export default Input;

