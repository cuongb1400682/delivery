import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    underline: {
        borderStyle: "solid",
        borderBottomColor: "rgba(255, 255, 255, 0.8)",
        borderBottomWidth: 1
    },
    inputContainer: {
        flexDirection: "row",
        alignItems: "center",
        height: 36
    },
    iconContainer: {
        height: 36,
        width: 36,
        alignItems: "center",
        justifyContent: "center",
        marginRight: 8
    },
    input: {
        color: "#fff",
        flex: 1
    },
});

export {
    styles
};
