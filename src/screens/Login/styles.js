import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    loginScreen: {
        flex: 1,
        paddingHorizontal: 28,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
    },
    idInput: {
        marginBottom: 8
    },
    backgroundImage: {
        opacity: 0.08,
    },
    overlayBackground: {
        backgroundColor: "#F76001",
    },
    forgotPassword: {
        color: "#fff",
        alignSelf: "flex-end"
    },
    forgotPasswordContainer: {
        alignSelf: "flex-end",
        marginVertical: 20
    },
    loginButton: {
        backgroundColor: "#fff",
        height: 36,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "stretch",
        borderRadius: 8
    },
    loginButtonText: {
        color: colors.primaryColor
    },
    appLogo: {
        marginBottom: 36
    }
});

export {
    styles
};
