import React, {Component} from "react";
import {withNavigation} from "react-navigation";
import {Image, Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import {styles} from "./styles";
import {loginWithGoogle} from "src/redux/common/userAuthActions";
import {isUserLoggedIn} from "src/utils/userAuth";
import shipper from "src/assets/static/images/shipper.jpg";
import {usefulStyles} from "src/styles/usefulStyles";
import AppLogo from "src/screens/Login/components/AppLogo/AppLogo";
import Input from "src/screens/Login/components/Input/Input";
import {tr} from "src/localization/localization";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import screenIds from "src/constants/screenIds";
import GiigaaLinearGradient from "src/components/base/GiigaaLinearGradient/GiigaaLinearGradient";

@withNavigation
class Login extends Component {
    static propTypes = {
        isUserLoggingIn: PropTypes.bool
    };

    static defaultProps = {
        isUserLoggingIn: true
    };

    componentDidMount() {
        const {navigation} = this.props;

        if (isUserLoggedIn()) {
            navigation.navigate(screenIds.ORDERS_LIST);
        }
    }

    render() {
        const {isUserLoggingIn} = this.props;

        return (
            <View style={styles.loginScreen}>
                <GiigaaLinearGradient style={[usefulStyles.fillDeviceScreen, styles.overlayBackground]}/>
                <Image
                    resizeMode="cover"
                    resizeMethod="resize"
                    style={[usefulStyles.fillDeviceScreen, styles.backgroundImage]}
                    source={shipper}
                />
                <AppLogo style={styles.appLogo}/>
                <Input
                    inputContainerStyle={styles.idInput}
                    placeholder={tr("login_form_id")}
                    iconType="id"
                />
                <Input
                    placeholder={tr("login_form_password")}
                    iconType="password"
                    secureTextEntry={true}
                />
                <TouchableOpacity style={styles.forgotPasswordContainer}>
                    <Text style={styles.forgotPassword}>
                        {tr("login_forgot_password")}
                    </Text>
                </TouchableOpacity>
                <Touchable style={styles.loginButton}>
                    <Text style={styles.loginButtonText}>{tr("login_button")}</Text>
                </Touchable>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    isUserLoggingIn: state.user_auth.isLoggingIn
});

export default connect(mapStateToProps, {
    loginWithGoogle
})(Login);
