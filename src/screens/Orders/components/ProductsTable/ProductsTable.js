import React, {Component} from "react";
import {FlatList, Image, Text, View} from "react-native";
import PropTypes from "prop-types";

import {styles} from "./styles";
import {formatNumber, formatWithCurrency} from "src/utils/strings";
import giftIcon from "src/assets/static/icons/gift.png";
import {tr} from "src/localization/localization";

class ProductsTable extends Component {
    static propTypes = {
        orderDetail: PropTypes.object,
    };

    static defaultProps = {
        orderDetail: {}
    };

    renderProductItem = ({item, index}) => {
        const {
            item_qty: itemQty,
            price: itemPrice,
            product = {}
        } = item;

        const {
            name: productName,
            original_thumbnail: originalThumbnail // TODO: unexisted, wait for api to return it
        } = product;

        const showGiftIcon = false;

        return (
            <View style={styles.productItemContainer} key={index}>
                <Image
                    style={styles.productItemThumbnail}
                    source={originalThumbnail}
                />
                <View style={styles.productItemNameContainer}>
                    <Text style={styles.productItemText}>{productName}</Text>
                </View>
                <View style={styles.productItemItemQtyContainer}>
                    <Text style={styles.productItemText}>x{formatNumber(itemQty, 0)}</Text>
                </View>
                <View style={styles.productItemItemPriceContainer}>
                    <Text style={styles.productItemText}>{formatWithCurrency(itemPrice)}</Text>
                    {showGiftIcon && <Image style={styles.giftIcon} source={giftIcon}/>}
                </View>
            </View>
        )
    };

    renderProductsTableSeparator = () => <View style={styles.productsTableSeparator}/>;

    renderProductsTableEmpty = () => (
        <View style={styles.productsTableEmptyTextContainer}>
            <Text>{tr("order_detail_products_table_empty")}</Text>
        </View>
    );

    renderTotalLines = () => {
        const {
            so_lines = [],
            shipping_fee = 0,
            sub_total = 0,
            total = 0
        } = this.props.orderDetail;

        return (
            <View style={[styles.orderDetailSection, styles.whiteBackground, styles.hasTopBorder]}>
                <View style={styles.totalLine}>
                    <Text style={styles.infoText}>{tr("products_table_temporary_price")}</Text>
                    <Text>{formatWithCurrency(sub_total)}</Text>
                </View>
                <View style={[styles.totalLine, styles.middleTotalLine]}>
                    <Text style={styles.infoText}>{tr("products_table_shipping_fee")}</Text>
                    <Text>{formatWithCurrency(shipping_fee)}</Text>
                </View>
                <View style={styles.totalLine}>
                    <Text style={[styles.infoText, styles.blackText]}>
                        {tr("products_table_total")} ({so_lines.length} {tr("products_table_product")})
                    </Text>
                    <View>
                        <Text style={styles.totalMoney}>{formatWithCurrency(total)}</Text>
                        <Text style={styles.vatIncluded}>{tr("products_table_vat_included")}</Text>
                    </View>
                </View>
            </View>
        );
    };

    render() {
        const {so_lines = []} = this.props.orderDetail;

        return (
            <View style={styles.productsTableContainer}>
                <Text style={[styles.boldText, styles.sectionTitle]}>{tr("products_table_title")}</Text>
                <FlatList
                    style={styles.productsTable}
                    data={so_lines}
                    renderItem={this.renderProductItem}
                    ItemSeparatorComponent={this.renderProductsTableSeparator}
                    ListEmptyComponent={this.renderProductsTableEmpty}
                    keyExtractor={(_, index) => index.toString()}
                />
                {this.renderTotalLines()}
            </View>
        )
    }
}

export default ProductsTable;
