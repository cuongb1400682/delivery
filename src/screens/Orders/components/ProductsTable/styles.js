import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    orderDetailSection: {
        paddingHorizontal: 0,
        paddingVertical: 14,
        borderStyle: "solid",
        backgroundColor: "#fff"
    },
    infoText: {
        color: "#757575",
        marginBottom: 4
    },
    blackText: {
        color: colors.textColor
    },
    productsTableContainer: {
        paddingHorizontal: 12,
        paddingVertical: 14,
        backgroundColor: "#fff",
        marginTop: 6
    },
    withoutBorder: {
        borderBottomWidth: 0
    },
    sectionTitle: {
        paddingBottom: 16
    },
    shippingStatus: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 4
    },
    boldText: {
        fontWeight: "bold",
        color: colors.textColor
    },
    productsTableSeparator: {
        borderStyle: "solid",
        borderBottomWidth: 1,
        borderBottomColor: "#fafafa"
    },
    productsTable: {
        backgroundColor: "#fff"
    },
    productsTableEmptyTextContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 16
    },
    productItemContainer: {
        marginVertical: 10,
        flexDirection: "row",
        flex: 1,
    },
    productItemThumbnail: {
        width: 48,
        height: 48,
        backgroundColor: "#dddddd",
        marginLeft: 0
    },
    productItemNameContainer: {
        flex: 1,
        marginLeft: 10,
    },
    productItemItemQtyContainer: {
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: "flex-start",
        marginLeft: 10,
        width: 30,
    },
    productItemItemPriceContainer: {
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "flex-end",
        marginLeft: 4,
        marginRight: 0,
        width: 80,
    },
    productItemText: {
        color: colors.textColor,
        fontSize: 14,
    },
    giftIcon: {
        width: 14,
        height: 14
    },
    hasTopBorder: {
        borderStyle: "solid",
        borderTopWidth: 1,
        borderTopColor: "#dddddd",
    },
    totalLine: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between"
    },
    totalMoney: {
        fontSize: 18,
        color: "#EF4323",
        alignSelf: "flex-end"
    },
    vatIncluded: {
        fontSize: 9,
        color: "#757575"
    },
    middleTotalLine: {
        marginVertical: 4
    },
    navButtonContainer: {
        paddingRight: 8
    }
});

export {
    styles
};
