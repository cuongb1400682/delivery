import React, {Component} from "react";
import {Text, View} from "react-native";
import PropTypes from "prop-types";

import {styles} from "./styles";
import {convertOrderStepToPaymentStatus} from "src/utils/orderStep";
import {toShortDateDisplay} from "src/utils/dateTime";
import {tr} from "src/localization/localization";
import {makeFullAddress} from "src/utils/strings";

class OrderInfo extends Component {
    static propTypes = {
        orderDetail: PropTypes.object,
    };

    static defaultProps = {
        orderDetail: {}
    };

    render() {
        const {order_number, created_time, payment_method, order_step, customer = {}, shipping_address = {}} = this.props.orderDetail;
        const paymentStatus = convertOrderStepToPaymentStatus(order_step);

        return (
            <View style={[styles.orderInfoSection, styles.orderDetailSection]}>
                <Text style={styles.infoText}>
                    {tr("order_info_order_number")}
                    <Text style={styles.boldText}>#{order_number}</Text>
                </Text>
                <Text style={styles.infoText}>
                    {tr("order_info_created_time")}
                    {toShortDateDisplay(created_time)}
                </Text>
                <Text style={styles.infoText}>
                    {tr("order_info_transporter")}
                    <Text style={styles.blackText}>
                        Giigaa.com
                    </Text>
                </Text>
                <Text style={styles.infoText}>
                    {tr("order_info_payment_method")}
                    <Text style={styles.blackText}>
                        {tr(`order_detail_payment_method_${payment_method}`)}
                    </Text>
                </Text>
                <Text style={styles.infoText}>
                    {tr("order_info_payment_status")}
                    <Text style={styles.blackText}>
                        {tr(`order_detail_payment_status_${paymentStatus}`)}
                    </Text>
                </Text>
                <View style={styles.separator}/>
                <Text style={[styles.boldText, styles.sectionTitle]}>
                    {tr("order_info_billing_info_title")}
                </Text>
                <Text style={[styles.infoText, styles.boldText]}>
                    {customer.name}
                </Text>
                <Text style={[styles.infoText]}>
                    {tr("order_info_shipping_address")}
                    {makeFullAddress(shipping_address)}
                </Text>
                <Text style={[styles.infoText]}>
                    {tr("order_info_contact_phone")}
                    {shipping_address.contact_phone}
                </Text>
            </View>
        );
    }
}

export default OrderInfo;

