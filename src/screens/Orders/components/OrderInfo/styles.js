import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    orderInfoSection: {
        backgroundColor: "#f9f9f9",
    },
    orderDetailSection: {
        paddingHorizontal: 12,
        paddingVertical: 14,
        borderStyle: "solid",
        borderBottomWidth: 1,
        borderBottomColor: "#dddddd",
        backgroundColor: "#fff"
    },
    sectionTitle: {
        paddingBottom: 4
    },
    orderNumberLabel: {
        marginBottom: 4
    },
    boldText: {
        fontWeight: "bold",
        color: colors.textColor
    },
    infoText: {
        color: "#757575",
        marginBottom: 4
    },
    blackText: {
        color: colors.textColor
    },
    whiteBackground: {
        backgroundColor: "#fff"
    },
    separator: {
        borderStyle: "solid",
        borderBottomWidth: 1,
        borderBottomColor: "#ddd",
        marginVertical: 10
    }
});

export {
    styles
};
