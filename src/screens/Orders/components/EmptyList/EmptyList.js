import React from "react";
import {Text, View} from "react-native";

import {styles} from "./styles";
import {tr} from "src/localization/localization";

const EmptyList = () => (
    <View style={styles.ordersListEmpty}>
        <Text>{tr("orders_list_empty")}</Text>
    </View>
);

export default EmptyList;
