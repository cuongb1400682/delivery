import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    ordersListEmpty: {
        flex: 1,
        marginTop: 50,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#fff"
    }
});

export {
    styles
};
