import React, {Component} from "react";
import {Text, View} from "react-native";
import PropTypes from "prop-types";
import DraggableFlatList from "react-native-draggable-flatlist";

import {styles} from "./styles";
import Separator from "src/screens/Orders/components/Separator/Separator";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import EmptyList from "src/screens/Orders/components/EmptyList/EmptyList";
import {toShortDateDisplay} from "src/utils/dateTime";
import {formatWithCurrency} from "src/utils/strings";
import HelperButton from "src/screens/Orders/components/HelperButton/HelperButton";
import GiigaaLinearGradient from "src/components/base/GiigaaLinearGradient/GiigaaLinearGradient";

class TaskList extends Component {
    static propTypes = {
        refreshing: PropTypes.bool,
        dataSource: PropTypes.array,
        onItemPress: PropTypes.func
    };

    static defaultProps = {
        refreshing: false,
        dataSource: [],
        onItemPress: () => {
        }
    };

    renderTaskListItem = ({item, index, move, moveEnd}) => {
        const {id: order_id, order_number, created_time, payment_method, total, customer_name} = item;
        const isDateChanged = false;

        return (
            <GiigaaLinearGradient colors={isDateChanged ? ["#EF4423", "#F56300"] : ["#fff", "#fff"]}>
                <Touchable
                    onPress={() => this.props.onItemPress(order_id)}
                    onLongPress={move}
                    onPressOut={moveEnd}
                >
                    <View style={styles.listItem} key={index}>
                        <View style={styles.itemIndex}>
                            <Text style={styles.itemIndexText}>#{index + 1}</Text>
                        </View>
                        <View>
                            <Text style={styles.orderInfo}>
                                {order_number}
                                <Text style={styles.verticalLine}>{" | "}</Text>
                                {customer_name}
                            </Text>
                            <Text style={styles.shippingInfo}>
                                {toShortDateDisplay(created_time)}
                                <Text style={styles.verticalLine}>{" | "}</Text>
                                {payment_method}
                            </Text>
                            <Text style={styles.totalLine}>
                                {formatWithCurrency(total)}
                            </Text>
                        </View>
                        <View style={styles.helperButtonsContainer}>
                            <HelperButton style={styles.helperButton} type="2h"/>
                            <HelperButton style={styles.helperButton} type="cellphone"/>
                            <HelperButton style={styles.helperButton} type="marker"/>
                        </View>
                    </View>
                </Touchable>
            </GiigaaLinearGradient>
        )
    };

    updateNewTaskOrder = ({data}) => {
        // TODO: update new order
    };

    render() {
        return (
            <DraggableFlatList
                style={styles.taskList}
                refreshing={this.props.refreshing}
                data={this.props.dataSource}
                ItemSeparatorComponent={() => <Separator/>}
                ListEmptyComponent={() => <EmptyList/>}
                renderItem={this.renderTaskListItem}
                keyExtractor={({order_number}) => order_number}
                onMoveEnd={this.updateNewTaskOrder}
            />
        );
    }
}

export default TaskList;
