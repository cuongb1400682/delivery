import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    taskList: {
        flex: 1,
        backgroundColor: "#EBEEF3",
        paddingHorizontal: 16,
        paddingVertical: 8
    },
    listItem: {
        backgroundColor: "transparent",
        paddingHorizontal: 10,
        paddingVertical: 16,
        flexDirection: "row"
    },
    itemIndex: {
        width: 30,
        height: 30,
        backgroundColor: "#EAEDF2",
        borderRadius: 30,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginRight: 10
    },
    itemIndexText: {
        color: colors.textColor,
        fontSize: 11
    },
    orderInfo: {
        fontSize: 12,
        fontWeight: "bold",
        color: "#000"
    },
    shippingInfo: {
        fontSize: 11,
        color: "#757575",
        paddingVertical: 3
    },
    totalLine: {
        fontSize: 12,
        color: colors.primaryColor,
        fontWeight: "bold"
    },
    verticalLine: {
        fontWeight: "normal",
        fontSize: 14,
    },
    helperButtonsContainer: {
        position: "absolute",
        flexDirection: "row",
        bottom: 10,
        right: 16
    },
    helperButton: {
        marginLeft: 12
    }
});

export {
    styles
};
