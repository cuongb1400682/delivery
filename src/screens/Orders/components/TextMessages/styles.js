import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    textMessageContent: {
        flex: 1,
        flexDirection: "column",
        height: 100,
        paddingBottom: 32
    },
    radioGroup: {
        paddingBottom: 20
    },
    radio: {
        marginVertical: 20
    },
    separator: {
        backgroundColor: "#c8c7cc",
        width: "100%",
        height: 0.5
    },
    textSendButton: {
        height: 36,
        flex: 1,
        backgroundColor: colors.primaryColor,
        borderRadius: 8
    },
    kiteIcon: {
        color: "#fff",
        marginRight: 8,
        fontSize: 12
    },
    textSendButtonText: {
        color: "#fff",
        fontWeight: "bold"
    },
    buttonContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 16,
        width: "100%"
    }
});

export {
    styles
};
