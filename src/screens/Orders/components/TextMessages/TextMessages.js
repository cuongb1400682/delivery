import React, {Component} from "react";
import PropTypes from "prop-types";
import {Text, View} from "react-native";

import {styles} from "./styles";
import {tr} from "src/localization/localization";
import Radio from "src/components/base/Radio/Radio";
import RadioGroup from "src/components/base/RadioGroup/RadioGroup";
import UpwardSlidingModal from "src/components/base/UpwardSlidingModal/UpwardSlidingModal";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {usefulStyles} from "src/styles/usefulStyles";
import {FontAwesome} from "@expo/vector-icons";

const Separator = () => <View style={styles.separator}/>;

class TextMessages extends Component {
    static propTypes = {
        onTextSelected: PropTypes.func,
        visible: PropTypes.bool,
        closeModal: PropTypes.func,
    };

    static defaultProps = {
        visible: false,
        onTextSelected: () => {
        },
        closeModal: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedMessageIndex: -1
        };
    }

    selectMessage = (selectedMessageIndex) => {
        this.setState({
            selectedMessageIndex
        });
    };

    sendTextMessage = () => {
        // TODO: invoke api to send text message
    };

    render() {
        return (
            <UpwardSlidingModal
                title={tr("text_messages_modal_title")}
                visible={this.props.visible}
                closeModal={this.props.closeModal}
                headerStyle={styles.radioGroup}
            >
                <Separator/>
                <RadioGroup onChildrenPress={this.selectMessage}>
                    <Radio style={styles.radio}>{tr("text_messages_option_1")}</Radio>
                    <Separator/>
                    <Radio style={styles.radio}>{tr("text_messages_option_2")}</Radio>
                </RadioGroup>
                <Separator/>
                <View style={styles.buttonContainer}>
                    <Touchable
                        style={[styles.textSendButton, usefulStyles.flexRowCenter]}
                         onPress={this.sendTextMessage}
                    >
                        <FontAwesome style={styles.kiteIcon} name="send"/>
                        <Text style={styles.textSendButtonText}>{tr("text_messages_send_button_text")}</Text>
                    </Touchable>
                </View>
            </UpwardSlidingModal>
        );
    }
}

export default TextMessages;
