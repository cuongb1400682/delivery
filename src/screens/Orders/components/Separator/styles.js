import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    ordersListSeparator: {
        height: 4,
        width: "100%",
        backgroundColor: "#EBEEF3"
    }
});

export {
    styles
};
