import React, {Component} from "react";
import {Text, View} from "react-native";
import {FontAwesome} from "@expo/vector-icons";
import PropTypes from "prop-types";

import {styles} from "./styles"
import {usefulStyles} from "src/styles/usefulStyles";
import {tr} from "src/localization/localization";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import UpwardSlidingModal from "src/components/base/UpwardSlidingModal/UpwardSlidingModal";

class SupportingCalls extends Component {
    static propTypes = {
        visible: PropTypes.bool,
        phoneList: PropTypes.array,
        emergencyPhone: PropTypes.string,
        onMakePhoneCall: PropTypes.func,
        closeModal: PropTypes.func
    };

    static defaultProps = {
        visible: false,
        phoneList: [],
        emergencyPhone: "",
        onMakePhoneCall: () => {
        },
        closeModal: () => {
        }
    };

    makePhoneCall = (phone) => () => this.props.onMakePhoneCall(phone);

    renderSupportingCallItem = ({name, phone}, index) => (
        <View key={index} style={[styles.supportingCallItem, styles.topBorder]}>
            <Text>{name}</Text>
            <Touchable style={styles.phoneTouchable} onPress={this.makePhoneCall(phone)}>
                <FontAwesome style={styles.phoneIcon} name="phone"/>
            </Touchable>
        </View>
    );

    render() {
        return (
            <UpwardSlidingModal
                closeModal={this.props.closeModal}
                visible={this.props.visible}
                title={tr("supporting_calls_modal_title")}
                description={tr("supporting_calls_modal_description")}
            >
                {this.props.phoneList.map(this.renderSupportingCallItem)}
                <View style={[styles.topBorder, styles.emergencyContainer]}>
                    <Touchable
                        style={[styles.emergencyCallButton, usefulStyles.flexRowCenter]}
                        onPress={this.makePhoneCall(this.props.emergencyPhone)}
                    >
                        <FontAwesome style={styles.emergencyPhoneIcon} name="phone"/>
                        <Text style={styles.emergencyText}>{tr("supporting_calls_button_text")}</Text>
                    </Touchable>
                </View>
            </UpwardSlidingModal>
        );
    }
}

export default SupportingCalls;
