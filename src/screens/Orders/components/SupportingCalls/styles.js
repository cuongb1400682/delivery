import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    phoneTouchable: {
        width: 36,
        height: 36,
        borderRadius: 36,
        borderColor: "#dedede",
        borderWidth: 0.5,
        borderStyle: "solid",
        alignItems: "center",
        justifyContent: "center"
    },
    phoneIcon: {
        fontSize: 18,
        color: "#007aff"
    },
    supportingCallItem: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        height: 55.5,
    },
    emergencyCallButton: {
        height: 36,
        flex: 1,
        backgroundColor: colors.primaryColor,
        borderRadius: 8
    },
    topBorder: {
        borderStyle: "solid",
        borderColor: "#c8c7cc",
        borderTopWidth: 0.5,
    },
    emergencyPhoneIcon: {
        color: "#fff",
        marginRight: 8,
        fontSize: 12
    },
    emergencyText: {
        color: "#fff",
        fontWeight: "bold"
    },
    emergencyContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 16,
        width: "100%"
    }
});

export {
    styles
};
