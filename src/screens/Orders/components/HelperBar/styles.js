import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    helperBar: {
        backgroundColor: "#fff",
        height: 60,
        flexDirection: "row",
        alignSelf: "stretch",
        alignItems: "center",
        justifyContent: "space-between",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 1,
        zIndex: 1,
        paddingHorizontal: 16
    },
    completeButton: {
        width: 124,
        height: 40,
        backgroundColor: "#4CD964",
        borderRadius: 26,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    completeButtonText: {
        color: "#fff"
    },
    circleButtonsContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center"
    },
    helperButton: {
        marginLeft: 14
    }
});

export {
    styles
};
