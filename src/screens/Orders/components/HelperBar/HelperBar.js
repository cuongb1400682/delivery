import React, {Component} from "react";
import {Text, View} from "react-native";
import PropTypes from "prop-types";

import {styles} from "./styles";
import HelperButton from "src/screens/Orders/components/HelperButton/HelperButton";
import {tr} from "src/localization/localization";

class HelperBar extends Component {
    static propTypes = {
        onSendMessage: PropTypes.func,
        onMakePhoneCall: PropTypes.func,
        onShowMap: PropTypes.func,
    };

    static defaultProps = {
        onSendMessage: () => {
        },
        onMakePhoneCall: () => {
        },
        onShowMap: () => {
        }
    };

    render() {
        return (
            <View style={styles.helperBar}>
                <View style={styles.completeButton}>
                    <Text style={styles.completeButtonText}>
                        {tr("helper_bar_complete_button_text")}
                    </Text>
                </View>
                <View style={styles.circleButtonsContainer}>
                    <HelperButton
                        style={styles.helperButton}
                        size={32}
                        onPress={this.props.onSendMessage}
                        type="message"
                    />
                    <HelperButton
                        style={styles.helperButton}
                        size={32}
                        onPress={this.props.onMakePhoneCall}
                        type="cellphone"
                    />
                    <HelperButton
                        style={styles.helperButton}
                        size={32}
                        onPress={this.props.onShowMap}
                        type="marker"
                    />
                </View>
            </View>
        );
    }
}

export default HelperBar;
