import {createAsyncAction} from "src/utils/createAction";
import ordersActionTypes from "src/redux/actionTypes/ordersActionTypes";
import {SOListTimeSortType} from "src/constants/enums";
import {endOfDay, startOfDay} from "src/utils/dateTime";

const fetchOrdersList = () => {
    const from_time = startOfDay().toISOString();
    const to_time = endOfDay().toISOString();

    return createAsyncAction({
        type: ordersActionTypes.FETCH_ORDERS_LIST,
        payload: {
            request: "/sales_order/so/search",
            method: "GET",
            params: {
                from_time: from_time,
                to_time: to_time,
                time_type: SOListTimeSortType.MODIFIED,
                store_id: -1,
                order_source: "ALL",
                status: "ALL",
                order_number: ""
            }
        }
    });
};

const fetchOrderDetail = (order_id) => createAsyncAction({
    type: ordersActionTypes.FETCH_ORDER_DETAIL,
    payload: {
        method: "GET",
        request: "/sales_order/pos/get_detail",
        params: {
            id: order_id
        }
    }
});

export {
    fetchOrdersList,
    fetchOrderDetail
};
