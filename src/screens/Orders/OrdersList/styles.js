import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    boldText: {
        fontWeight: "bold",
        color: "#000"
    },
    tabBarUnderline: {
        height: 3,
        backgroundColor: colors.primaryColor
    },
    tabActiveText: {
        color: "#000",
        fontWeight: "normal"
    }
});

export {
    styles
};
