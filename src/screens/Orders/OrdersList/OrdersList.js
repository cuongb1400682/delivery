import React, {Component} from "react";
import {View} from "react-native";
import {withNavigation} from "react-navigation";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {Tab, Tabs} from "native-base";

import {styles} from "./styles";
import {fetchOrdersList} from "src/screens/Orders/ordersActions";
import {hideDestinationsMap, hideSupportingCalls} from "src/redux/common/commonActions";
import screenIds from "src/constants/screenIds";
import TaskList from "src/screens/Orders/components/TaskList/TaskList";
import OrderDetail from "src/screens/Orders/OrderDetail/OrderDetail";
import SupportingCalls from "src/screens/Orders/components/SupportingCalls/SupportingCalls";
import DestinationsMap from "src/components/base/DestinationsMap/DestinationsMap";
import {tr} from "src/localization/localization";

const fakeDestinations = [{
    "longitude": 106.70247695331422,
    "latitude": 10.730414133472909
}, {"longitude": 106.70284794625712, "latitude": 10.729519972289065}, {
    "longitude": 106.70213896157199,
    "latitude": 10.728612847809154
}, {"longitude": 106.70154632236813, "latitude": 10.729795868392745}, {
    "longitude": 106.7032777465864,
    "latitude": 10.729137872860859
}, {"longitude": 106.70229734535285, "latitude": 10.729593110129542}];

const fakeCurrentDestination = {
    "latitude": 10.821316832625484,
    "longitude": 106.6326901626763,
};

const fakePhoneList = [{
    name: "CS",
    phone: "0924888444"
}, {
    name: "Điều phối",
    phone: "0222888445"
}];

@withNavigation
class OrdersList extends Component {
    static propTypes = {
        fetchOrdersList: PropTypes.func,

        inProgressList: PropTypes.array,
        isFetchingInProgressTask: PropTypes.bool,
        hasErrorWhenFetchingInProgressTask: PropTypes.bool,

        newTaskList: PropTypes.array,
        isFetchingNewTask: PropTypes.bool,
        hasErrorWhenFetchingNewTask: PropTypes.bool,

        doneTaskList: PropTypes.array,
        isFetchingDoneTask: PropTypes.bool,
        hasErrorWhenFetchingDoneTask: PropTypes.bool,

        isSupportingCallVisible: PropTypes.bool,
        hideSupportingCalls: PropTypes.func,

        isDestinationsMapVisible: PropTypes.bool,
        hideDestinationsMap: PropTypes.func
    };

    static defaultProps = {
        fetchOrdersList: () => {
        },

        inProgressList: [],
        isFetchingInProgressTask: false,
        hasErrorWhenFetchingInProgressTask: false,

        newTaskList: [],
        isFetchingNewTask: false,
        hasErrorWhenFetchingNewTask: false,

        doneTaskList: [],
        isFetchingDoneTask: false,
        hasErrorWhenFetchingDoneTask: false,

        isSupportingCallVisible: false,
        hideSupportingCalls: () => {
        },

        isDestinationsMapVisible: false,
        hideDestinationsMap: () => {
        }
    };

    componentDidMount() {
        this.props.fetchOrdersList();
    }

    showOrderDetailWithId = (orderId) => {
        this.props.navigation.navigate(screenIds.ORDER_DETAIL, {
            [OrderDetail.navParams.orderId]: orderId
        });
    };

    renderTabs = () => {
        const {
            isFetchingInProgressTask,
            inProgressList,
            isFetchingNewTask,
            newTaskList,
            isFetchingDoneTask,
            doneTaskList,
        } = this.props;

        return (
            <Tabs
                tabBarUnderlineStyle={styles.tabBarUnderline}
                tabContainerStyle={{backgroundColor: "#fff"}}
            >
                <Tab activeTextStyle={styles.tabActiveText} heading="Mới gửi">
                    <TaskList
                        refreshing={isFetchingNewTask}
                        dataSource={newTaskList}
                        onItemPress={this.showOrderDetailWithId}
                    />
                </Tab>
                <Tab activeTextStyle={styles.tabActiveText} heading="Đang làm">
                    <TaskList
                        refreshing={isFetchingInProgressTask}
                        dataSource={inProgressList}
                        onItemPress={this.showOrderDetailWithId}
                    />
                </Tab>
                <Tab activeTextStyle={styles.tabActiveText} heading="Đã xong">
                    <TaskList
                        refreshing={isFetchingDoneTask}
                        dataSource={doneTaskList}
                        onItemPress={this.showOrderDetailWithId}
                    />
                </Tab>
            </Tabs>
        );
    };

    render() {
        return (
            <View style={{flex: 1}}>
                {this.renderTabs()}
                <SupportingCalls
                    visible={this.props.isSupportingCallVisible}
                    closeModal={this.props.hideSupportingCalls}
                    phoneList={fakePhoneList}
                />
                <DestinationsMap
                    visible={this.props.isDestinationsMapVisible}
                    currentPosition={fakeCurrentDestination}
                    destinations={fakeDestinations}
                    title={tr("all_destination_title")}
                    description={`${fakeDestinations.length} ${tr("all_destination_description")}`}
                    closeModal={this.props.hideDestinationsMap}
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    inProgressList: state.ordersRouteConfigs.orders_list.data,
    isFetchingInProgressTask: state.ordersRouteConfigs.orders_list.fetching,
    hasErrorWhenFetchingInProgressTask: state.ordersRouteConfigs.orders_list.error,

    newTaskList: state.ordersRouteConfigs.orders_list.data,
    isFetchingNewTask: state.ordersRouteConfigs.orders_list.fetching,
    hasErrorWhenFetchingNewTask: state.ordersRouteConfigs.orders_list.error,

    doneTaskList: state.ordersRouteConfigs.orders_list.data,
    isFetchingDoneTask: state.ordersRouteConfigs.orders_list.fetching,
    hasErrorWhenFetchingDoneTask: state.ordersRouteConfigs.orders_list.error,

    isSupportingCallVisible: state.common.supporting_calls_visible,
    isDestinationsMapVisible: state.common.all_destination_visible
});

export default connect(mapStateToProps, {fetchOrdersList, hideSupportingCalls, hideDestinationsMap})(OrdersList);
