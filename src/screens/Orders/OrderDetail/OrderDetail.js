import React, {Component} from "react";
import {withNavigation} from "react-navigation";
import {ActionSheetIOS, ActivityIndicator, ScrollView, Text, View} from "react-native";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import {styles} from "./styles";
import {fetchOrderDetail} from "src/screens/Orders/ordersActions";
import {tr} from "src/localization/localization";
import {errorCodes} from "src/constants/errorCodes";
import OrderInfo from "src/screens/Orders/components/OrderInfo/OrderInfo";
import ProductsTable from "src/screens/Orders/components/ProductsTable/ProductsTable";
import HelperBar from "src/screens/Orders/components/HelperBar/HelperBar";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import DoubleCheck from "src/components/base/SvgIcons/DoubleCheck/DoubleCheck";
import {usefulStyles} from "src/styles/usefulStyles";
import screenIds from "src/constants/screenIds";
import TextMessages from "src/screens/Orders/components/TextMessages/TextMessages";

@withNavigation
class OrderDetail extends Component {
    static propTypes = {
        orderDetail: PropTypes.object,
        isFetchingOrderDetail: PropTypes.bool,
        hasErrorWhenFetchingOrderDetail: PropTypes.bool,
        fetchOrderDetail: PropTypes.func,
    };

    static defaultProps = {
        orderDetail: {},
        isFetchingOrderDetail: false,
        hasErrorWhenFetchingOrderDetail: false,
        fetchOrderDetail: () => {
        },
    };

    static navParams = {
        orderId: "orderId",
    };

    constructor(props) {
        super(props);

        this.state = {
            isTextMessagesVisible: false
        };
    }


    componentDidMount() {
        const {navigation, fetchOrderDetail} = this.props;
        const orderId = navigation.getParam(OrderDetail.navParams.orderId, 0);

        fetchOrderDetail(orderId);
    }

    toggleTextMessageVisibility = () => {
        this.setState({
            isTextMessagesVisible: !this.state.isTextMessagesVisible
        })
    };

    showOrderStatusUpdateActionSheet = () => {
        ActionSheetIOS.showActionSheetWithOptions({
            title: tr("order_detail_order_status_update_title"),
            options: [
                tr("order_detail_order_status_update_complete"),
                tr("order_detail_order_status_update_cancel")
            ],
            cancelButtonIndex: 1
        }, pressedButtonIndex => {
            if (pressedButtonIndex === 0) {
                // TODO: wait for api to change order status
                this.props.navigation.push(screenIds.CUSTOMER_SIGNATURE);
            }
        })
    };

    sendTextMessage = (text) => {
        // TODO: invoke API to send text message
    };

    render() {
        if (this.props.isFetchingOrderDetail) {
            return (
                <View style={{flex: 1}}>
                    <ActivityIndicator animating={true}/>
                </View>
            );
        }

        if (this.props.hasErrorWhenFetchingOrderDetail) {
            return (
                <View style={{flex: 1}}>
                    <Text>{tr(errorCodes.errorLoadingFailedToLoadData)}</Text>
                </View>
            )
        }

        return (
            <View style={{flex: 1}}>
                <HelperBar onSendMessage={this.toggleTextMessageVisibility}/>
                <ScrollView style={styles.orderDetailContainer}>
                    <OrderInfo orderDetail={this.props.orderDetail}/>
                    <ProductsTable orderDetail={this.props.orderDetail}/>
                    <Touchable style={styles.updateButtonTouchable} onPress={this.showOrderStatusUpdateActionSheet}>
                        <View style={[usefulStyles.flexRowCenter, styles.updateButton]}>
                            <DoubleCheck style={styles.doubleCheckIcon}/>
                            <Text style={styles.updateButtonText}>{tr("order_detail_update_button_title")}</Text>
                        </View>
                    </Touchable>
                </ScrollView>
                <TextMessages
                    visible={this.state.isTextMessagesVisible}
                    closeModal={this.toggleTextMessageVisibility}
                    onTextSelected={this.sendTextMessage}
                />
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    orderDetail: state.ordersRouteConfigs.order_detail.data,
    isFetchingOrderDetail: state.ordersRouteConfigs.order_detail.fetching,
    hasErrorWhenFetchingOrderDetail: state.ordersRouteConfigs.order_detail.error,
});

export default connect(mapStateToProps, {fetchOrderDetail})(OrderDetail);
