import {StyleSheet} from "react-native";

import colors from "src/constants/colors";

const styles = StyleSheet.create({
    orderDetailContainer: {
        paddingHorizontal: 16,
        paddingVertical: 8
    },
    navButtonContainer: {
        paddingRight: 8
    },
    updateButton: {
        height: 36,
        backgroundColor: colors.primaryColor,
        borderRadius: 8
    },
    updateButtonText: {
        color: "#fff",
        marginLeft: 8
    },
    doubleCheckIcon: {
    },
    updateButtonTouchable: {
        marginTop: 8,
        marginBottom: 16
    }
});

export {
    styles
};
