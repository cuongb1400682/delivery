import {StyleSheet} from "react-native";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    customerSignature: {
        flex: 1,
        backgroundColor: "transparent",
        flexDirection: "column",
        justifyContent: "space-around",
        alignItems: "center"
    },
    roundedButton: {
        height: 40,
        backgroundColor: colors.primaryColor,
        borderRadius: 26,
        justifyContent: "center",
        alignItems: "center"
    },
    roundedButtonText: {
        color: "#fff",
        fontSize: 15
    },
    buttonsContainer: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        marginHorizontal: 16
    },
    button: {
        alignSelf: "stretch",
        flex: 1
    },
    placeholder: {
        width: 16
    }
});

export {
    styles
};
