import React, {Component} from "react";
import {connect} from "react-redux";
import {Text, View} from "react-native";
import {withNavigation} from "react-navigation";

import {styles} from "./styles";
import SignatureBoard from "src/components/base/SignatureBoard/SignatureBoard";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";
import {tr} from "src/localization/localization";
import {screenHeight, screenWidth} from "src/utils/dimensions";

const RoundedButton = ({onPress, title, style}) => (
    <Touchable style={[styles.roundedButton, style]} onPress={onPress}>
        <Text style={styles.roundedButtonText}>
            {title}
        </Text>
    </Touchable>
);

@withNavigation
class CustomerSignature extends Component {
    clearBoard = () => {
        this.signatureBoardRef && this.signatureBoardRef.clear();
    };

    uploadSignature = async () => {
        if (this.signatureBoardRef) {
            const base64Signature = await this.signatureBoardRef.convertToBase64();
            // TODO: invoke api to upload the signature
        }
    };

    render() {
        const boardWidth = screenWidth() - 20;
        const boardHeight = screenHeight() - 190;

        return (
            <View style={styles.customerSignature}>
                <SignatureBoard
                    ref={ref => this.signatureBoardRef = ref}
                    width={boardWidth}
                    height={boardHeight}
                />
                <View style={styles.buttonsContainer}>
                    <RoundedButton
                        onPress={this.uploadSignature}
                        style={styles.button}
                        title={tr("customer_signature_button_ok")}
                    />
                    <View style={styles.placeholder}/>
                    <RoundedButton
                        onPress={this.clearBoard}
                        style={styles.button}
                        title={tr("customer_signature_button_clear")}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps, {})(CustomerSignature);
