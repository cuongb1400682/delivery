import orderActionTypes from "src/redux/actionTypes/ordersActionTypes";
import {toFailure, toRequest, toSuccess} from "src/utils/createAction";
import {addOrderStepToOrdersList, convertToOrderManagerStep} from "src/utils/orderStep";

const initialState = {
    orders_list: {
        data: [],
        fetching: false,
        error: false
    },
    order_detail: {
        data: {},
        fetching: false,
        error: false
    }
};

const ordersReducers = (state = initialState, {type, payload}) => {
    switch (type) {
        case toRequest(orderActionTypes.FETCH_ORDERS_LIST):
            return {
                ...state,
                orders_list: {
                    ...state.orders_list,
                    fetching: true,
                    error: false,
                }
            };
        case toFailure(orderActionTypes.FETCH_ORDERS_LIST):
            return {
                ...state,
                orders_list: {
                    ...state.orders_list,
                    fetching: false,
                    error: true,
                }
            };
        case toSuccess(orderActionTypes.FETCH_ORDERS_LIST):
            return {
                ...state,
                orders_list: {
                    ...state.orders_list,
                    fetching: false,
                    error: false,
                    data: addOrderStepToOrdersList(payload.reply)
                }
            };
        case toRequest(orderActionTypes.FETCH_ORDER_DETAIL):
            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    data: {},
                    fetching: true,
                    error: false
                }
            };
        case toFailure(orderActionTypes.FETCH_ORDER_DETAIL):
            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    data: {},
                    fetching: false,
                    error: true
                }
            };
        case toSuccess(orderActionTypes.FETCH_ORDER_DETAIL):
            return {
                ...state,
                order_detail: {
                    ...state.order_detail,
                    data: {
                        ...payload.reply,
                        order_step: convertToOrderManagerStep(payload.reply)
                    },
                    fetching: false,
                    error: false
                }
            };
        default:
            return state;
    }
};

export default ordersReducers;
