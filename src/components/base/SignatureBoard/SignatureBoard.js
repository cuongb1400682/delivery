import React, {Component} from "react";
import {PanResponder} from "react-native";
import {Svg} from "expo";
import PropTypes from "prop-types";

import {styles} from "./styles";
import {errorCodes} from "src/constants/errorCodes";

const POINT_RECOGNITION_DELAY_TIME = 5;

class SignatureBoard extends Component {
    static propTypes = {
        style: PropTypes.any,
        width: PropTypes.number,
        height: PropTypes.number,
        stroke: PropTypes.string,
        strokeWidth: PropTypes.number
    };

    static defaultProps = {
        style: {},
        width: 500,
        height: 500,
        stroke: "black",
        strokeWidth: 2
    };

    constructor(props) {
        super(props);

        this.state = {
            shapes: [""]
        };
        this.touchedPoint = null;
        this.prevTouchedPoint = null;
        this.hasUserMoved = false;
        this.panResponder = PanResponder.create(this);
    }

    componentDidMount() {
        this.timer = setInterval(this.drawLastestStroke, POINT_RECOGNITION_DELAY_TIME);
    }

    componentWillUnmount() {
        if (this.timer) {
            clearInterval(this.timer);
        }
    }

    drawLastestStroke = () => {
        if (this.prevTouchedPoint) {
            const {shapes} = this.state;
            const shapesLength = shapes.length;
            const lastestShape = shapes[shapesLength - 1];
            const [prevX, prevY] = this.prevTouchedPoint;
            const [currX, currY] = this.touchedPoint;
            const newStroke = `M${prevX} ${prevY} L${currX} ${currY}`;

            if (this.hasUserMoved) {
                this.setState({
                    shapes: [
                        ...shapes.slice(0, shapesLength - 1),
                        lastestShape + newStroke
                    ]
                })
            }
        }

        this.prevTouchedPoint = this.touchedPoint;
        this.hasUserMoved = false;
    };

    onStartShouldSetPanResponder = () => true;

    onStartShouldSetPanResponderCapture = () => true;

    onMoveShouldSetPanResponder = () => true;

    onMoveShouldSetPanResponderCapture = () => true;

    onPanResponderTerminationRequest = () => true;

    onPanResponderGrant = () => {
        const {shapes} = this.state;
        const shapesLength = shapes.length;

        this.prevTouchedPoint = null;
        this.touchedPoint = null;

        if (shapes[shapesLength - 1].length > 0) {
            this.setState({
                shapes: [
                    ...shapes,
                    ""
                ]
            });
        }
    };

    onPanResponderMove = (evt) => {
        if (evt.nativeEvent) {
            const {locationX = -1, locationY = -1} = evt.nativeEvent;

            if (locationX >= 0) {
                this.touchedPoint = [locationX, locationY];
                this.hasUserMoved = true;
            }
        }
    };

    clear = () => {
        this.touchedPoint = null;
        this.prevTouchedPoint = null;
        this.hasUserMoved = false;
        this.setState({
            shapes: [""]
        });
    };

    undo = () => {
        let newShapes = [
            ...this.state.shapes
        ];

        newShapes.pop();
        if (newShapes.length === 0) {
            newShapes = [""];
        }

        this.setState({
            shapes: [
                ...newShapes
            ]
        });
    };

    convertToBase64 = () => new Promise((resolve, reject) => {
        if (!this.svgRef) {
            reject(errorCodes.errorUndefinedSVGRef);
        }

        if (!this.svgRef.toDataURL) {
            reject(errorCodes.errorMethodToDataURLNotFound)
        }

        this.svgRef.toDataURL(resolve);
    });

    render() {
        return (
            <Svg
                ref={ref => this.svgRef = ref}
                {...this.panResponder.panHandlers}
                style={[styles.whiteBoard, this.props.style]}
                width={this.props.width}
                height={this.props.height}
            >
                {this.state.shapes.map((path, index) =>
                    <Svg.Path
                        key={index}
                        d={path}
                        stroke={this.props.stroke}
                        strokeWidth={this.props.strokeWidth}
                        fill="none"
                    />
                )}
            </Svg>
        );
    }
}

export default SignatureBoard;
