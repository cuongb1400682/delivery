import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    whiteBoard: {
        backgroundColor: "#fff",
    }
});

export {
    styles
};
