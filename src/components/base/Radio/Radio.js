import React, {Component} from "react";
import {Text, View} from "react-native";
import PropTypes from "prop-types";

import {styles} from "./styles";
import colors from "src/constants/colors";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";

class Radio extends Component {
    static propTypes = {
        color: PropTypes.string,
        selected: PropTypes.bool,
        children: PropTypes.any,
        style: PropTypes.any
    };

    static defaultProps = {
        color: colors.primaryColor,
        selected: false,
        children: null,
        style: {}
    };

    render() {
        const {color, selected, children, style} = this.props;

        const outterCircleStyles = [styles.outterCircle, {borderColor: color}];
        const innerCircleStyles = [styles.innerCircle, {backgroundColor: color}];

        return (
            <Touchable style={style} onPress={this.props.onPress}>
                <View style={styles.radioContent}>
                    <View style={outterCircleStyles}>
                        {selected && <View style={innerCircleStyles}/>}
                    </View>
                    {typeof children === "string"
                        ? <Text style={styles.radioText}>{children}</Text> : children}
                </View>
            </Touchable>
        );
    }
}

export default Radio;
