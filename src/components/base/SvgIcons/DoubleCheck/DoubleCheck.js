import React from "react";
import {Svg} from "expo";

const DoubleCheck = (props) => {
    return (
        <Svg {...props} width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Svg.Path
                d="M14.9999 5.83333L13.8249 4.65833L8.54155 9.94166L9.71655 11.1167L14.9999 5.83333ZM18.5332 4.65833L9.71655 13.475L6.23322 9.99999L5.05822 11.175L9.71655 15.8333L19.7166 5.83333L18.5332 4.65833ZM0.341553 11.175L4.99989 15.8333L6.17489 14.6583L1.52489 9.99999L0.341553 11.175Z"
                fill="white"/>
        </Svg>
    );
};

export default DoubleCheck;


