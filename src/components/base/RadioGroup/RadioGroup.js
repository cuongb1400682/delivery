import React, {Component} from "react";
import {View} from "react-native";
import PropTypes from "prop-types";

class RadioGroup extends Component {
    static propTypes = {
        children: PropTypes.any.isRequired,
        onChildrenPress: PropTypes.func,
        style: PropTypes.any
    };

    static defaultProps = {
        children: [],
        onChildrenPress: () => {
        },
        style: {}
    };

    constructor(props) {
        super(props);

        this.state = {
            selectedChildIndex: -1
        };
    }

    onChildrenPress = (index) => () => {
        this.setState({
            selectedChildIndex: index
        });

        this.props.onChildrenPress(index);
    };

    render() {
        return (
            <View style={this.props.style}>
                {React.Children.map(this.props.children, (child, index) =>
                    React.cloneElement(child, {
                        onPress: this.onChildrenPress(index),
                        selected: this.state.selectedChildIndex === index
                    })
                )}
            </View>
        );
    }
}

export default RadioGroup;
