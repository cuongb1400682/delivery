import React, {Component} from "react";
import {Modal, StyleSheet, Text, TouchableWithoutFeedback, View} from "react-native";
import {FontAwesome} from "@expo/vector-icons";
import PropTypes from "prop-types";

import {styles} from "./styles"
import {usefulStyles} from "src/styles/usefulStyles";
import Touchable from "src/components/platformSpecific/Touchable/Touchable";

class UpwardSlidingModal extends Component {
    static propTypes = {
        visible: PropTypes.bool,
        closeModal: PropTypes.func,
        children: PropTypes.any,
        title: PropTypes.string,
        description: PropTypes.string,
        contentStyle: PropTypes.any,
        headerStyle: PropTypes.any,
        spaceFromTop: PropTypes.number,
        onShow: PropTypes.func,
        onClose: PropTypes.func
    };

    static defaultProps = {
        visible: false,
        closeModal: () => {
        },
        children: null,
        title: "",
        description: "",
        contentStyle: {},
        headerStyle: {},
        spaceFromTop: 0,
        onShow: () => {
        },
        onClose: () => {
        }
    };

    closeModal = () => this.props.closeModal();

    render() {
        return (
            <React.Fragment>
                {this.props.visible &&
                <TouchableWithoutFeedback onPress={this.closeModal}>
                    <View style={[StyleSheet.absoluteFill, styles.overlay]}/>
                </TouchableWithoutFeedback>}
                <Modal
                    visible={this.props.visible}
                    animationType="slide"
                    hardwareAccelerated
                    transparent
                    onShow={this.props.onShow}
                    onDismiss={this.props.onClose}
                >
                    <View style={styles.modal}>
                        <View style={styles.modalContent}>
                            <View style={[styles.modalHeader, usefulStyles.flexDirectionColumn, this.props.headerStyle]}>
                                <View style={styles.dragHandle}/>
                                <Text style={styles.modalTitle}>{this.props.title}</Text>
                                {this.props.description &&
                                <Text style={styles.modalDescription}>{this.props.description}</Text>}
                                <Touchable style={styles.closeButtonTouchable} onPress={this.closeModal}>
                                    <FontAwesome style={styles.closeButton} name="times-circle"/>
                                </Touchable>
                            </View>
                            <View style={[styles.content, this.props.contentStyle]}>
                                {this.props.children}
                            </View>
                        </View>
                    </View>
                </Modal>
            </React.Fragment>
        );
    }
}

export default UpwardSlidingModal;
