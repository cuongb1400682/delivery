import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    modal: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "flex-end"
    },
    modalContent: {
        flexDirection: "column"
    },
    overlay: {
        backgroundColor: "rgba(25, 25, 25, 0.5)",
        flex: 1
    },
    placeholder: {
        flex: 1
    },
    modalHeader: {
        backgroundColor: "#fff",
        paddingHorizontal: 16,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderStyle: "solid",
        borderColor: "#CDCED2",
        paddingTop: 16
    },
    content: {
        width: "100%",
        backgroundColor: "#fff",
        paddingHorizontal: 16,
    },
    dragHandle: {
        width: 36,
        height: 3.5,
        backgroundColor: "rgba(203, 205, 204, 0.5)",
        borderRadius: 100,
        alignSelf: "center",
        marginTop: 4,
        position: "absolute"
    },
    modalTitle: {
        lineHeight: 24,
        fontSize: 18,
        letterSpacing: 0.25,
        color: "#111",
        fontWeight: "bold"
    },
    modalDescription: {
        lineHeight: 16,
        fontSize: 13,
        letterSpacing: 0.06,
        color: "#8c8c8c",
        width: 240,
        marginBottom: 20
    },
    closeButtonTouchable: {
        position: "absolute",
        height: 52,
        width: 52,
        top: 0,
        right: 0,
        borderRadius: 48,
        alignItems: "center",
        justifyContent: "center"
    },
    closeButton: {
        fontSize: 24,
        color: "#dadada",
    },
});

export {
    styles
};
