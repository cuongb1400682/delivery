import React, {Component} from "react";
import {Location, MapView, Permissions, Svg} from "expo";
import PropTypes from "prop-types";

import {styles} from "./styles";
import UpwardSlidingModal from "src/components/base/UpwardSlidingModal/UpwardSlidingModal";
import {hoChiMinh} from "src/constants/locations";
import {Text, View} from "react-native";
import colors from "src/constants/colors";
import {tr} from "src/localization/localization";
import Message from "src/components/platformSpecific/Message/Message";

const DestinationMarker = ({coordinate, color = colors.primaryColor, children, onPress, ...props}) => (
    <MapView.Marker style={styles.marker} onPress={onPress} coordinate={coordinate} {...props}>
        <View style={[styles.markerTouchable, {backgroundColor: color}]}>
            {children}
        </View>
        <View style={[styles.triangle, {borderBottomColor: color}]}/>
        <View style={[styles.point, {backgroundColor: color}]}/>
    </MapView.Marker>
);

class DestinationsMap extends Component {
    static propTypes = {
        title: PropTypes.string,
        description: PropTypes.string,
        destinations: PropTypes.array,
        onMarkerPress: PropTypes.func,
        visible: PropTypes.bool,
        closeModal: PropTypes.func
    };

    static defaultProps = {
        title: "",
        description: "",
        destinations: [],
        onMarkerPress: () => {
        },
        visible: false,
        closeModal: () => {
        }
    };

    constructor(props) {
        super(props);

        this.state = {
            currentPosition: {
                longitude: hoChiMinh.longitude,
                latitude: hoChiMinh.latitude
            },
            altitudeParams: {
                longitudeDelta: hoChiMinh.longitudeDelta,
                latitudeDelta: hoChiMinh.latitudeDelta
            }
        };
    }

    componentWillUnmount() {
        this.unsubcribeLocationMonitor();
    }

    onMarkerPress = (coordinate) => () => {
        this.props.onMarkerPress(coordinate);
    };

    unsubcribeLocationMonitor = () => {
        if (this.subscription && typeof this.subscription.remove === "function") {
            this.subscription.remove();
        }
    };

    subscribeLocationMonitor = async () => {
        const result = await Permissions.askAsync(Permissions.LOCATION);

        if (result.status !== "granted") {
            Message.show(tr("location_perm_failed_to_grant"));
            return;
        }

        this.subscription = await Location.watchPositionAsync({
            enableHighAccuracy: true,
            timeInterval: 200
        }, this.updateCurrentPosition);
    };

    getCurrentRegion = () => {
        const {currentPosition, altitudeParams} = this.state;

        return {
            longitude: currentPosition.longitude,
            latitude: currentPosition.latitude,
            latitudeDelta: altitudeParams.latitudeDelta,
            longitudeDelta: altitudeParams.longitudeDelta,
        };
    };

    moveToCurrentRegion = () => {
        if (this.mapViewRef) {
            this.mapViewRef.animateToRegion(this.getCurrentRegion());
        }
    };

    updateCurrentPosition = ({coords}) => {
        this.setState({
            currentPosition: coords
        }, this.moveToCurrentRegion);
    };

    updateAltitudeParams = ({latitudeDelta, longitudeDelta}) => {
        this.setState({
            altitudeParams: {
                longitudeDelta,
                latitudeDelta
            }
        });
    };

    renderDestinations = () => this.props.destinations.map((coordinate, index) =>
        <DestinationMarker onPress={this.onMarkerPress(coordinate)} key={index} coordinate={coordinate}>
            <Text style={styles.markerText}>
                #{index + 1}
            </Text>
        </DestinationMarker>
    );

    renderCurrentLocation = () => (
        this.state.currentPosition && <DestinationMarker
            color="#007aff"
            coordinate={this.state.currentPosition}
            onPress={this.onMarkerPress(this.state.currentPosition)}
        >
            <Svg width="18" height="19" viewBox="0 0 18 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                <Svg.Path
                    d="M9.12108 19C6.13453 19 3.14798 19 0.161435 19H0.0807175C0.0807175 19 0 19 0 18.9208C0.0807175 17.9708 0.484305 17.1 1.21076 16.3875C1.69507 15.9125 2.34081 15.5958 2.90583 15.3583C3.55157 15.1208 4.19731 14.8833 4.76233 14.5667C5.16592 14.4083 5.56951 14.1708 5.97309 13.9333C6.29596 13.6958 6.53812 13.4583 6.61883 13.1417C6.69955 12.9833 6.69955 12.9042 6.69955 12.7458C6.69955 12.1917 6.69955 11.7167 6.69955 11.1625C6.69955 11.0833 6.69955 11.0833 6.61883 11.0042C6.4574 10.8458 6.29596 10.6875 6.21525 10.5292C5.89238 10.0542 5.73094 9.5 5.65022 8.94583C5.65022 8.86667 5.56951 8.7875 5.48879 8.70833C5.24664 8.62917 5.16592 8.47083 5.00448 8.3125C4.84305 8.075 4.84305 7.75833 4.76233 7.52083C4.76233 7.28333 4.76233 7.04583 4.84305 6.80833C5.00448 6.80833 5.0852 6.72917 5.24664 6.65V6.57083C5.16592 6.25417 5.0852 5.9375 5.00448 5.62083C4.92377 4.9875 4.84305 4.43333 4.84305 3.8C4.84305 3.24583 4.92377 2.69167 5.16592 2.21667C5.48879 1.26667 6.21525 0.633333 7.18386 0.316667C7.74888 0.0791667 8.3139 0 8.95964 0C9.44395 0 10.009 0 10.4126 0.158333C10.7354 0.2375 11.0583 0.475 11.2197 0.7125C11.2197 0.791667 11.3004 0.791667 11.3004 0.791667C11.7848 0.870833 12.2691 1.02917 12.6726 1.425C12.9955 1.74167 13.2377 2.1375 13.3184 2.6125C13.4798 3.0875 13.4798 3.64167 13.4798 4.11667C13.3991 4.90833 13.2377 5.77917 12.9955 6.49167V6.57083C13.2377 6.72917 13.3184 6.96667 13.3184 7.28333C13.3184 7.75833 13.2377 8.15417 12.9955 8.55C12.9148 8.70833 12.7534 8.86667 12.5112 8.86667C12.4305 8.86667 12.4305 8.86667 12.4305 8.94583C12.3498 9.42083 12.1883 9.89583 12.0269 10.2917C11.8655 10.5292 11.7848 10.7667 11.5426 11.0042C11.4619 11.0833 11.4619 11.0833 11.3812 11.1625C11.3004 11.1625 11.3004 11.2417 11.3004 11.2417C11.3004 11.7167 11.3004 12.2708 11.3004 12.7458C11.3004 13.3 11.5426 13.6167 11.9462 13.9333C12.2691 14.25 12.7534 14.4083 13.157 14.5667C13.722 14.8042 14.3677 15.0417 14.9327 15.2792C15.4978 15.5167 15.9821 15.7542 16.4664 16.0708C17.0314 16.4667 17.435 16.9417 17.6771 17.4958C17.8386 17.8917 17.9193 18.2875 18 18.6833C18 18.7625 18 18.8417 18 18.9208C18 19 18 19 17.9193 19H17.8386C15.0942 19 12.1076 19 9.12108 19Z"
                    fill="white"
                />
            </Svg>
        </DestinationMarker>
    );

    render() {
        return (
            <UpwardSlidingModal
                visible={this.props.visible}
                contentStyle={styles.destinationsMap}
                title={this.props.title}
                description={this.props.description}
                closeModal={this.props.closeModal}
                onShow={this.subscribeLocationMonitor}
                onClose={this.unsubcribeLocationMonitor}
            >
                <MapView
                    ref={ref => this.mapViewRef= ref}
                    style={styles.mapView}
                    initialRegion={hoChiMinh}
                    onRegionChangeComplete={this.updateAltitudeParams}
                    provide="google"
                    showsCompass
                    showsTraffic
                    showsIndoors
                    showsMyLocationButton
                    showsIndoorLevelPicker
                >
                    {this.renderDestinations()}
                    {this.renderCurrentLocation()}
                    {this.props.children}
                </MapView>
            </UpwardSlidingModal>
        );
    }
}

DestinationsMap.Marker = DestinationMarker;

export default DestinationsMap;
