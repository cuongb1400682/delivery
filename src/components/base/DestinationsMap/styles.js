import {StyleSheet} from "react-native";
import {screenHeight} from "src/utils/dimensions";
import colors from "src/constants/colors";

const styles = StyleSheet.create({
    destinationsMap: {
        paddingHorizontal: 0
    },
    mapView: {
        height: screenHeight() - 150
    },
    markerTouchable: {
        height: 34,
        width: 34,
        borderRadius: 34,
        backgroundColor: colors.primaryColor,
        justifyContent: "center",
        alignItems: "center"
    },
    markerText: {
        color: "#fff",
        fontSize: 13
    },
    triangle: {
        position: "absolute",
        bottom: -3.5,
        width: 0,
        height: 0,
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderBottomWidth: 3.5,
        borderStyle: 'solid',
        backgroundColor: "transparent",
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderBottomColor: colors.primaryColor,
        transform: [
            {rotate: "180deg"}
        ],
        zIndex: -1
    },
    point: {
        position: "absolute",
        bottom: -8,
        height: 3.4,
        width: 3.4,
        borderRadius: 3.4,
        backgroundColor: colors.primaryColor
    },
    marker: {
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center"
    },
    currentPositionMarker: {

    }
});

export {
    styles
};
