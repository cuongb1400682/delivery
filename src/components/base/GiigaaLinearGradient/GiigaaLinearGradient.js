import React from "react";
import {LinearGradient} from "expo";

const GiigaaLinearGradient = ({children, ...props}) => (
    <LinearGradient colors={["#ef4423", "#f56300"]} {...props}>
        {children}
    </LinearGradient>
);

export default GiigaaLinearGradient;
