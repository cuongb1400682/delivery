import {TouchableOpacity, TouchableNativeFeedback, Platform} from "react-native";

export default Platform.select({
    ios: TouchableOpacity,
    android: TouchableNativeFeedback
});
