import {DangerZone} from "expo";

const {Localization} = DangerZone;

const DEFAULT_LOCALE = "vi_VN";

const dictionaries = ({
    "en": require("./strings/en"),
    "vi": require("./strings/vi"),
});

let locale = DEFAULT_LOCALE;

const getSuitableDictionary = (currentLocale = DEFAULT_LOCALE) => {
    const language = currentLocale.substr(0, 2);

    if (!dictionaries[language]) {
        console.warn(`"${language}.json" not found. Use the default ${DEFAULT_LOCALE.substr(0, 2)}.json`);
        return dictionaries[DEFAULT_LOCALE.substr(0, 2)];
    } else {
        return dictionaries[language];
    }
};

const tr = (string = "") => {
    const localizedString = string.toLowerCase();
    const currentLocale = locale || DEFAULT_LOCALE;
    const dict = getSuitableDictionary(currentLocale);

    if (!dict[localizedString]) {
        console.warn(`"${localizedString}" is not translated in ${currentLocale}.json. Return the string itself.`);
        return localizedString;
    }

    return dict[localizedString];
};

const initializeLocalization = async () => {
    try {
        locale = await Localization.getCurrentLocaleAsync();
    } catch (e) {
        console.error(e.message);
        locale = DEFAULT_LOCALE;
    }
};

export {
    tr,
    initializeLocalization
};
