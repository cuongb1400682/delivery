const {NODE_ENV = "development"} = process.env;

let configs = ({
    "development": require("./developmentConfigs").default,
    "production": require("./productionConfigs").default
})[NODE_ENV];

if (NODE_ENV === "development") {
    console.log("=================================================");
    console.log("In Development Mode");
    console.log(configs);
    console.log("=================================================");
}

export default {
    ...configs
};
