import commonActionTypes from "src/redux/actionTypes/commonActionTypes";

const initialState = {
    supporting_calls_visible: false,
    all_destination_visible: false,
};

const commonReducers = (state = initialState, {type, payload}) => {
    switch (type) {
        case commonActionTypes.SHOW_DESTINATIONS_MAP:
            return {
                ...state,
                all_destination_visible: true
            };
        case commonActionTypes.HIDE_DESTINATIONS_MAP:
            return {
                ...state,
                all_destination_visible: false
            };
        case commonActionTypes.SHOW_SUPPORTING_CALL:
            return {
                ...state,
                supporting_calls_visible: true
            };
        case commonActionTypes.HIDE_SUPPORTING_CALL:
            return {
                ...state,
                supporting_calls_visible: false
            };
        default:
            return state;
    }
};

export default commonReducers;
