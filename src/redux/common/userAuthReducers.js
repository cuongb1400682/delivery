import userAuthActionTypes from "src/redux/actionTypes/userAuthActionTypes";
import {toFailure, toRequest, toSuccess} from "src/utils/createAction";

const initialState = {
    accessToken: "",
    isLoggingIn: false,
    isLoggingOut: false,
    userProfile: {}
};

const userAuthReducer = (state = initialState, {type, payload}) => {
    switch (type) {
        case toRequest(userAuthActionTypes.DELETE_ACCESS_TOKEN):
            return {
                ...state,
                isLoggingOut: true
            };
        case toSuccess(userAuthActionTypes.DELETE_ACCESS_TOKEN):
        case toFailure(userAuthActionTypes.DELETE_ACCESS_TOKEN):
            return {
                ...state,
                isLoggingOut: false
            };

        case toRequest(userAuthActionTypes.SEND_SERVER_AUTH_CODE):
            return {
                ...state,
                isLoggingIn: true,
            };
        case toSuccess(userAuthActionTypes.SEND_SERVER_AUTH_CODE):
            return {
                ...state,
                accessToken: payload.access_token,
                isLoggingIn: false,
            };
        case toFailure(userAuthActionTypes.SEND_SERVER_AUTH_CODE):
            return {
                ...state,
                isLoggingIn: false,
            };

        case userAuthActionTypes.SAVE_USER_PROFILE:
            return {
                ...state,
                userProfile: {
                    email: payload.email,
                    name: payload.name,
                    photoUrl: payload.photoUrl
                }
            };

        case userAuthActionTypes.CLEAR_USER_AUTH_DATA:
            return {
                ...initialState
            };

        default:
            return state;
    }
};

export default userAuthReducer;
