import {createAction} from "src/utils/createAction";
import commonActionTypes from "src/redux/actionTypes/commonActionTypes";

const showSupportingCalls = () => createAction({
    type: commonActionTypes.SHOW_SUPPORTING_CALL,
    payload: {}
});

const hideSupportingCalls = () => createAction({
    type: commonActionTypes.HIDE_SUPPORTING_CALL,
    payload: {}
});

const showDestinationsMap = () => createAction({
    type: commonActionTypes.SHOW_DESTINATIONS_MAP,
    payload: {}
});

const hideDestinationsMap = () => createAction({
    type: commonActionTypes.HIDE_DESTINATIONS_MAP,
    payload: {}
});

export {
    showSupportingCalls,
    hideSupportingCalls,
    showDestinationsMap,
    hideDestinationsMap
}
