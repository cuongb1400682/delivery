import Expo from "expo";
import configs from "src/configs/configs";

import Message from "src/components/platformSpecific/Message/Message";
import {createAction, createAsyncAction} from "src/utils/createAction";
import userAuthActionTypes from "src/redux/actionTypes/userAuthActionTypes";

const sendServerAuthCode = (serverAuthCode) => createAsyncAction({
    type: userAuthActionTypes.SEND_SERVER_AUTH_CODE,
    payload: {
        request: "/api/auth/google/sign_in",
        method: "POST",
        body: {
            code: serverAuthCode
        },
        options: {
            isAuth: true
        }
    }
});

const saveUserProfile = (profile) => createAction({
    type: userAuthActionTypes.SAVE_USER_PROFILE,
    payload: {
        email: profile.email,
        name: profile.name,
        photoUrl: profile.photoUrl
    }
});

const loginWithGoogle = () => async (dispatch) => {
    try {
        const response = await Expo.Google.logInAsync({
            androidClientId: configs.GOOGLE_CLIENT_ID,
            iosClientId: configs.GOOGLE_CLIENT_ID,
            scopes: ['profile', 'email']
        });

        const {serverAuthCode, user} = response;

        dispatch(sendServerAuthCode(serverAuthCode));
        dispatch(saveUserProfile(user));
    } catch (e) {
        Message.show("Unable to login");
    }
};

const deleteAccessToken = () => createAsyncAction({
    type: userAuthActionTypes.DELETE_ACCESS_TOKEN,
    payload: {
        request: "/api/auth/google/sign_out",
        method: "POST",
        options: {
            isAuth: true
        }
    }
});

const clearUserAuthData = () => createAction({
    type: userAuthActionTypes.CLEAR_USER_AUTH_DATA,
    payload: {}
});

const logout = () => dispatch => {
    dispatch(clearUserAuthData());
    dispatch(deleteAccessToken());
};

export {
    loginWithGoogle,
    logout
};
