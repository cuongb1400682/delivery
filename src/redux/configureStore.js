import {applyMiddleware, compose, createStore} from 'redux'
import {persistReducer, persistStore} from 'redux-persist'
import {AsyncStorage} from "react-native";
import thunk from "redux-thunk";
import {createLogger} from "redux-logger"

import rootReducer from "src/redux/rootReducers";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    persistedReducer,
    {},
    composeEnhancers(
        applyMiddleware(thunk),
        applyMiddleware(createLogger())
    )
);

const persistor = persistStore(store);

export {
    store,
    persistor
};
