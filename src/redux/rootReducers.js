import {combineReducers} from "redux";

import userAuthReducers from "src/redux/common/userAuthReducers";
import ordersReducers from "src/screens/Orders/ordersReducers";
import commonReducers from "src/redux/common/commonReducers";

export default combineReducers({
    user_auth: userAuthReducers,
    ordersRouteConfigs: ordersReducers,
    common: commonReducers
});

