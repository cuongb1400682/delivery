export default {
    SHOW_SUPPORTING_CALL: "SHOW_SUPPORTING_CALL",
    HIDE_SUPPORTING_CALL: "HIDE_SUPPORTING_CALL",
    SHOW_DESTINATIONS_MAP: "SHOW_DESTINATIONS_MAP",
    HIDE_DESTINATIONS_MAP: "HIDE_DESTINATIONS_MAP",
};
