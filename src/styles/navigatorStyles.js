import {StyleSheet} from "react-native";

const navigatorStyles = StyleSheet.create({
    hideSystemDefaultHeader: {
        backgroundColor: "transparent",
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    bottomTab: {
        backgroundColor: "#fff",
        height: 50
    }
});

export {
    navigatorStyles
};
