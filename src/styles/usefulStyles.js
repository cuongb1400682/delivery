import {StyleSheet} from "react-native";
import {screenHeight, screenWidth} from "src/utils/dimensions";

const usefulStyles = StyleSheet.create({
    fillDeviceScreen: {
        position: "absolute",
        top: 0,
        left: 0,
        width: screenWidth(),
        height: screenHeight()
    },
    matchParent: {
        flex: 1
    },
    centerChildren: {
        justifyContent: "center",
        alignItems: "center"
    },
    crossAxisCenter: {
        alignItems: "center"
    },
    alongAxisCenter: {
        justifyContent: "center"
    },
    flexDirectionRow: {
        flexDirection: "row"
    },
    flexDirectionColumn: {
        flexDirection: "column"
    },
    flexRowCenter: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row"
    },
    flexColumnCenter: {
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column"
    }
});

export {
    usefulStyles
}
