import React, {Component} from "react";
import {PersistGate} from 'redux-persist/integration/react'
import {Provider} from "react-redux";
import {AppLoading, Font} from "expo";

import {persistor, store} from "./src/redux/configureStore";
import Root from "./src/navigator/createNavigator";
import {initializeLocalization} from "./src/localization/localization";
import {StatusBar} from "react-native";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            waitingForConfig: true
        };
    }


    async componentDidMount() {
        await initializeLocalization();
        await Font.loadAsync({
            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
        });

        this.setState({
            waitingForConfig: false
        });
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    {this.state.waitingForConfig
                        ? <AppLoading/>
                        : <Root/>}
                    <StatusBar barStyle="light-content"/>
                </PersistGate>
            </Provider>
        );
    }
}

export default App;
